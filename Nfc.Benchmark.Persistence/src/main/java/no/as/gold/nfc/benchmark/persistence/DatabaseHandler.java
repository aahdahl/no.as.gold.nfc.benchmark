package no.as.gold.nfc.benchmark.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;

import no.as.gold.nfc.benchmark.domain.IBatteryBenchmarkSample;
import no.as.gold.nfc.benchmark.domain.IComBenchmarkSample;
import no.as.gold.nfc.benchmark.domain.IRttSample;

/**
 * This class maps objects to a database. The class only handles putting objects to the database.
 * Created by Aage Dahl on 18.03.14.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 4;

    // Database Name
    private static final String DATABASE_NAME = "BenchmarkResults.db";

    // Contacts table name
    private static final String TABLE_READ_COMMUNICATION_PERFORMANCE = "ReadCommunicationPerformance";
    private static final String TABLE_WRITE_COMMUNICATION_PERFORMANCE = "WriteCommunicationPerformance";
    private static final String TABLE_READALL_COMMUNICATION_PERFORMANCE = "ReadAllCommunicationPerformance";
    private static final String TABLE_RTT = "RTT";
    private static final String TABLE_READ_BATTERY_PERFORMANCE = "ReadBatteryPerformance";
    private static final String TABLE_WRITE_BATTERY_PERFORMANCE = "WriteBatteryPerformance";
    private static final String TABLE_POWER_BATTERY_PERFORMANCE = "PowerBatteryPerformance";

    // general table attributes
    private static final String KEY_ID = "id";
    private static final String KEY_TIME = "time";
    private static final String KEY_BENCHMARK_NAME= "name";
    private static final String KEY_TECHNOLOGY = "technology";

    // communication table attributes
    private static final String KEY_SIZE = "size";
    private static final String KEY_DURATION = "duration";
    private static final String KEY_STATUS = "status";

    // battery table attributes
    private static final String KEY_START_LEVEL = "startLevel";
    private static final String KEY_END_LEVEL = "endLevel";
    private static final String KEY_ELAPSED_TIME = "elapsedTime";
    private static final String KEY_BYTES_TRANSFERRED = "bytesTransferred";

    // RTT table attributes
    private static final String KEY_RTT = "RTT";

    public DatabaseHandler(Context context) {
        super(context, Environment.getExternalStorageDirectory() + "/Nfc.Benchmark/" + DATABASE_NAME, null, DATABASE_VERSION);
    }

    //region SQLiteOpenHelper Overrides
    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        // Communication performance tables
        String CREATE_READ_COMMUNICATION_PERFORMANCE_TABLE =
                "CREATE TABLE IF NOT EXISTS " + TABLE_READ_COMMUNICATION_PERFORMANCE
                + "("
                + KEY_ID                + " INTEGER PRIMARY KEY,"
                + KEY_TIME              + " TEXT,"
                + KEY_BENCHMARK_NAME    + " TEXT,"
                + KEY_TECHNOLOGY        + " TEXT,"
                + KEY_SIZE              + " INTEGER,"
                + KEY_DURATION          + " FLOAT,"
                + KEY_STATUS            + " TEXT"
                + ")";

        String CREATE_WRITE_COMMUNICATION_PERFORMANCE_TABLE =
                "CREATE TABLE IF NOT EXISTS " + TABLE_WRITE_COMMUNICATION_PERFORMANCE
                + "("
                + KEY_ID                + " INTEGER PRIMARY KEY,"
                + KEY_TIME              + " TEXT,"
                + KEY_BENCHMARK_NAME    + " TEXT,"
                + KEY_TECHNOLOGY        + " TEXT,"
                + KEY_SIZE              + " INTEGER,"
                + KEY_DURATION          + " FLOAT,"
                + KEY_STATUS            + " TEXT"
                + ")";

        String CREATE_READALL_COMMUNICATION_PERFORMANCE_TABLE =
                "CREATE TABLE IF NOT EXISTS " + TABLE_READALL_COMMUNICATION_PERFORMANCE
                + "("
                + KEY_ID                + " INTEGER PRIMARY KEY,"
                + KEY_TIME              + " TEXT,"
                + KEY_BENCHMARK_NAME    + " TEXT,"
                + KEY_TECHNOLOGY        + " TEXT,"
                + KEY_SIZE              + " INTEGER,"
                + KEY_DURATION          + " FLOAT,"
                + KEY_STATUS            + " TEXT"
                + ")";

        // RTT performance tables
        String CREATE_RTT_TABLE =
                "CREATE TABLE IF NOT EXISTS " + TABLE_RTT
                + "("
                + KEY_ID                + " INTEGER PRIMARY KEY,"
                + KEY_TIME              + " TEXT,"
                + KEY_BENCHMARK_NAME    + " TEXT,"
                + KEY_TECHNOLOGY        + " TEXT,"
                + KEY_RTT               + " FLOAT"
                + ")";

        // Battery performance tables
        String CREATE_READ_BATTERY_PERFORMANCE_TABLE =
                "CREATE TABLE IF NOT EXISTS " + TABLE_READ_BATTERY_PERFORMANCE
                + "("
                + KEY_ID                + " INTEGER PRIMARY KEY,"
                + KEY_TIME              + " TEXT,"
                + KEY_BENCHMARK_NAME    + " TEXT,"
                + KEY_TECHNOLOGY        + " TEXT,"
                + KEY_START_LEVEL       + " FLOAT,"
                + KEY_END_LEVEL         + " FLOAT,"
                + KEY_ELAPSED_TIME      + " FLOAT,"
                + KEY_BYTES_TRANSFERRED + " INTEGER"
                + ")";

        String CREATE_WRITE_BATTERY_PERFORMANCE_TABLE =
                "CREATE TABLE IF NOT EXISTS " + TABLE_WRITE_BATTERY_PERFORMANCE
                + "("
                + KEY_ID                + " INTEGER PRIMARY KEY,"
                + KEY_TIME              + " TEXT,"
                + KEY_BENCHMARK_NAME    + " TEXT,"
                + KEY_TECHNOLOGY        + " TEXT,"
                + KEY_START_LEVEL       + " FLOAT,"
                + KEY_END_LEVEL         + " FLOAT,"
                + KEY_ELAPSED_TIME      + " FLOAT,"
                + KEY_BYTES_TRANSFERRED + " INTEGER"
                + ")";

        String CREATE_POWER_BATTERY_PERFORMANCE_TABLE =
                "CREATE TABLE IF NOT EXISTS " + TABLE_POWER_BATTERY_PERFORMANCE
                + "("
                + KEY_ID                + " INTEGER PRIMARY KEY,"
                + KEY_TIME              + " TEXT,"
                + KEY_BENCHMARK_NAME    + " TEXT,"
                + KEY_TECHNOLOGY        + " TEXT,"
                + KEY_START_LEVEL       + " FLOAT,"
                + KEY_END_LEVEL         + " FLOAT,"
                + KEY_ELAPSED_TIME      + " FLOAT,"
                + KEY_BYTES_TRANSFERRED + " INTEGER"
                + ")";


        db.execSQL(CREATE_READ_COMMUNICATION_PERFORMANCE_TABLE);
        db.execSQL(CREATE_WRITE_COMMUNICATION_PERFORMANCE_TABLE);
        db.execSQL(CREATE_READALL_COMMUNICATION_PERFORMANCE_TABLE);
        db.execSQL(CREATE_RTT_TABLE);
        db.execSQL(CREATE_READ_BATTERY_PERFORMANCE_TABLE);
        db.execSQL(CREATE_WRITE_BATTERY_PERFORMANCE_TABLE);
        db.execSQL(CREATE_POWER_BATTERY_PERFORMANCE_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        if(oldVersion == 3 && newVersion == 4) {
            // do nothing - a new table will be added, that's all...
        } else {
            // Drop older table if existed
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_READ_COMMUNICATION_PERFORMANCE);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_WRITE_COMMUNICATION_PERFORMANCE);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_READALL_COMMUNICATION_PERFORMANCE);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_RTT);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_READ_BATTERY_PERFORMANCE);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_WRITE_BATTERY_PERFORMANCE);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_POWER_BATTERY_PERFORMANCE);
        }
        // Create tables again
        onCreate(db);
    }
    //endregion SQLiteOpenHelper Overrides

    //region Public methods

    /**
     * This function removes all tables in the database
     */
    public void clearDatabase() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_READ_COMMUNICATION_PERFORMANCE, null, null);
        db.delete(TABLE_WRITE_COMMUNICATION_PERFORMANCE, null, null);
        db.delete(TABLE_RTT, null, null);
        db.delete(TABLE_READ_BATTERY_PERFORMANCE, null, null);
        db.delete(TABLE_WRITE_BATTERY_PERFORMANCE, null, null);
        db.delete(TABLE_POWER_BATTERY_PERFORMANCE, null, null);
        db.close();
    }

    /**
     * Adds a sample to the binary read benchmark table
     * @param sample Sample to be added
     */
    public void addReadCommunicationSample(IComBenchmarkSample sample) throws Exception {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = getCommunicationSampleContentValues(sample);
            // Inserting Row
            long rowId = db.insert(TABLE_READ_COMMUNICATION_PERFORMANCE, null, values);
            if (rowId < 0)
                throw new Exception("Unable to add sample to " + TABLE_READ_COMMUNICATION_PERFORMANCE);
        } finally {
            db.close(); // Closing database connection

        }
    }

    /**
     * Adds a sample to the binary read benchmark table
     * @param sample Sample to be added
     */
    public void addWriteCommunicationSample(IComBenchmarkSample sample) throws Exception {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = getCommunicationSampleContentValues(sample);
            // Inserting Row
            long rowId = db.insert(TABLE_WRITE_COMMUNICATION_PERFORMANCE, null, values);
            if (rowId < 0)
                throw new Exception("Unable to add sample to " + TABLE_WRITE_COMMUNICATION_PERFORMANCE);
        } finally {
            db.close(); // Closing database connection
        }
    }

    /**
     * Adds a sample to the binary read all benchmark table
     * @param sample Sample to be added
     */
    public void addReadAllCommunicationSample(IComBenchmarkSample sample) throws Exception {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = getCommunicationSampleContentValues(sample);
            // Inserting Row
            long rowId = db.insert(TABLE_READALL_COMMUNICATION_PERFORMANCE, null, values);
            if (rowId < 0)
                throw new Exception("Unable to add sample to " + TABLE_WRITE_COMMUNICATION_PERFORMANCE);
        } finally {
            db.close(); // Closing database connection
        }
    }

    /**
     * Adds a sample to the rtt table
     * @param sample Sample to be added
     */
    public void addRTTSample(IRttSample sample) throws Exception {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_TIME, sample.getTimeStamp().toString());
            values.put(KEY_BENCHMARK_NAME, sample.getBenchmarkName());
            values.put(KEY_TECHNOLOGY, sample.getTechnology());
            values.put(KEY_RTT, sample.getRttTime());

            // Inserting Row
            long rowId = db.insert(TABLE_RTT, null, values);
            if (rowId < 0) throw new Exception("Unable to add sample to " + TABLE_RTT);
        } finally {
            db.close(); // Closing database connection
        }
    }

    /**
     * Adds a sample to the battery read benchmark table
     * @param sample Sample to be added
     */
    public void addReadBatterySample(IBatteryBenchmarkSample sample) throws Exception {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = getBatterySampleContentValues(sample);

            // Inserting Row
            long rowId = db.insert(TABLE_READ_BATTERY_PERFORMANCE, null, values);
            if (rowId < 0)
                throw new Exception("Unable to add sample to " + TABLE_READ_BATTERY_PERFORMANCE);
        } finally {
            db.close(); // Closing database connection
        }
    }

    /**
     * Adds a sample to the battery write benchmark table
     * @param sample Sample to be added
     */
    public void addWriteBatterySample(IBatteryBenchmarkSample sample) throws Exception {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = getBatterySampleContentValues(sample);

            // Inserting Row
            long rowId = db.insert(TABLE_WRITE_BATTERY_PERFORMANCE, null, values);
            if (rowId < 0)
                throw new Exception("Unable to add sample to " + TABLE_WRITE_BATTERY_PERFORMANCE);
        } finally {
            db.close(); // Closing database connection
        }
    }

    /**
     * Adds a sample to the battery power benchmark table
     * @param sample Sample to be added
     */
    public void addPowerBatterySample(IBatteryBenchmarkSample sample) throws Exception {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = getBatterySampleContentValues(sample);

            // Inserting Row
            long rowId = db.insert(TABLE_POWER_BATTERY_PERFORMANCE, null, values);
            if (rowId < 0)
                throw new Exception("Unable to add sample to " + TABLE_POWER_BATTERY_PERFORMANCE);
        } finally {
            db.close(); // Closing database connection
        }
    }
    //endregion Public methods

    //region private methods

    /**
     * This function prepares the content values for a given sample
     * @param sample
     * @return
     */
    private ContentValues getCommunicationSampleContentValues(IComBenchmarkSample sample) {
        ContentValues values = new ContentValues();
        values.put(KEY_TIME, sample.getTimeStamp().toString());
        values.put(KEY_BENCHMARK_NAME, sample.getBenchmarkName());
        values.put(KEY_TECHNOLOGY, sample.getTechnology());
        values.put(KEY_SIZE, sample.getSampleSize());
        values.put(KEY_DURATION, sample.getSampleDuration());
        values.put(KEY_STATUS, sample.getSampleStatus().toString());
        return values;
    }

    private ContentValues getBatterySampleContentValues(IBatteryBenchmarkSample sample) {
        ContentValues values = new ContentValues();
        values.put(KEY_TIME, sample.getTimeStamp().toString());
        values.put(KEY_BENCHMARK_NAME, sample.getBenchmarkName());
        values.put(KEY_TECHNOLOGY, sample.getTechnology());
        values.put(KEY_ELAPSED_TIME ,sample.getElapsedTime());
        values.put(KEY_START_LEVEL ,sample.getStartLevel());
        values.put(KEY_END_LEVEL ,sample.getEndLevel());
        values.put(KEY_BYTES_TRANSFERRED, sample.getNumberOfBytesTransferred());
        return values;
    }

    //endregion
}

package no.as.gold.nfc.benchmark.persistence;

import android.os.Environment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

/**
 * Created by Aage Dahl on 19.03.14.
 */
public class ExportDatabaseUtility {
    //exporting database
    public static void exportDB() throws IOException {
        // TODO Auto-generated method stub

        File sd = Environment.getExternalStorageDirectory();
        File data = Environment.getDataDirectory();

        if(!sd.canWrite()) throw new IOException("Cannot write to External storage directory");

        String  currentDBPath= "/data/no.as.gold.nfc.benchmark.view.gui/databases/BenchmarkSampleCollector";
        String backupDBPath  = "/database.db";
        File currentDB = new File(data, currentDBPath);
        File backupDB = new File(sd, backupDBPath);

        FileChannel src = new FileInputStream(currentDB).getChannel();
        FileChannel dst = new FileOutputStream(backupDB).getChannel();
        dst.transferFrom(src, 0, src.size());
        src.close();
        dst.close();
    }

}

package no.as.gold.nfc.benchmark.domain;

import java.util.Date;

/**
 * Created by Aage Dahl on 11.03.14.
 */
public interface IBatteryBenchmarkSample {
    /**
     * Gets the name of the benchmark that this sample belongs to
     * @return Name of benchmark
     */
    public String getBenchmarkName();

    /**
     * Sets the name of the benchmark that this sample belongs to
     * @param name Name of benchmark
     */
    public void setBenchmarkName(String name);

    /**
     * Gets the timestamp of the sample
     * @return
     */
    public Date getTimeStamp();

    /**
     * Sets the time stamp of the sample
     * @param timeStamp time of sample
     */
    public void setTimeStamp(Date timeStamp);
    /**
     * Gets the technology that was used when collecting this sample.
     * @return Technology this sample was collected from
     */
    String getTechnology();

    /**
     * Describes this sample.
     * @return Description of the sample.
     */
    String getDescription();

    /**
     * The start level refers to the percentage of the battery level at the start of the benchmarking.
     * @return Battery level at the start of the benchmarking
     */
    double getStartLevel();

    /**
     * The end level refers to the percentage of the battery level at the end of the benchmarking.
     * @return Battery level at the end of the benchmarking.
     */
    double getEndLevel();

    /**
     * The amount of time from the benchmarking started until it ended in seconds
     * @return Elapsed time from start until the end of the benchmarking.
     */
    double getElapsedTime();

    /**
     * The amount of bytes transferred between the sender and the receiver.
     * @return Number of bytes transferred.
     */
    long getNumberOfBytesTransferred();

}

package no.as.gold.nfc.benchmark.domain;

import java.util.Date;

/**
 * Created by Aage Dahl on 20.03.14.
 */
public class DefaultRttSample implements IRttSample {
    private String mName;
    private Date mTimeStamp;
    private String mTechnology;
    private double mRttTime;

    @Override
    public String getBenchmarkName() {
        return mName;
    }

    @Override
    public void setBenchmarkName(String name) {
        mName = name;
    }

    @Override
    public Date getTimeStamp() {
        return mTimeStamp;
    }

    @Override
    public void setTimeStamp(Date timeStamp) {
        mTimeStamp = timeStamp;
    }

    @Override
    public String getTechnology() {
        return mTechnology;
    }

    @Override
    public void setTechnology(String technology) {
        mTechnology = technology;
    }

    @Override
    public double getRttTime() {
        return mRttTime;
    }

    @Override
    public void setRttTime(double rttTime) {
        mRttTime = rttTime;
    }
}

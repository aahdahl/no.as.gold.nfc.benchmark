package no.as.gold.nfc.benchmark.domain;

import java.util.Date;

/**
 * Created by aage on 13.12.13.
 */
public class DefaultComBenchmarkSample implements IComBenchmarkSample {
    //region Private fields
    private Date mTimeStamp;
    private int mSampleSize;
    private double mSampleDuration;
    private Status mSampleStatus;
    private String mTechnology;
    private String mName;
    //endregion

    //region Constructors

    /**
    * Extracts sample information from a message.
    * @param technology Technology communicated with.
    * @param timeStamp Time the sample was taken.
    * @param size Number of bytes transferred.
    * @param duration Transfer speed.
    * @param status Status of the transfer.
     */
    protected DefaultComBenchmarkSample(String technology, Date timeStamp, int size, double duration, Status status) {
        this("BenchmarkName", technology, timeStamp, size, duration, status);
    }

    /**
     * Extracts sample information from a message.
     * @param technology Technology communicated with.
     * @param timeStamp Time the sample was taken.
     * @param size Number of bytes transferred.
     * @param duration Transfer speed.
     * @param status Status of the transfer.
     */
    public DefaultComBenchmarkSample(String benchmarkName, String technology, Date timeStamp, int size, double duration, Status status) {
        mName = benchmarkName;
        mTechnology = technology;
        mTimeStamp = timeStamp;
        mSampleSize = size;
        mSampleDuration = duration;
        mSampleStatus = status;
    }
    //endregion

    //region IComBenchmarkSample implementation
    @Override
    public String getBenchmarkName() {
        return mName;
    }

    @Override
    public void setBenchmarkName(String name) {
        mName = name;
    }

    @Override
    public Date getTimeStamp() {
        return mTimeStamp;
    }

    @Override
    public void setTimeStamp(Date timeStamp) {
        mTimeStamp = timeStamp;
    }

    @Override
    public int getSampleSize() {
        return mSampleSize;
    }

    @Override
    public void setSampleSize(int sampleSize) {
        mSampleSize = sampleSize;
    }

    @Override
    public double getSampleDuration() {
        return mSampleDuration;
    }

    @Override
    public void setSampleDuration(double sampleDuration) {
        mSampleDuration = sampleDuration;
    }

    @Override
    public void setSampleStatus(Status status) {
        mSampleStatus = status;
    }

    @Override
    public Status getSampleStatus() {
        return mSampleStatus;
    }

    @Override
    public String getTechnology() {
        return mTechnology;
    }

    @Override
    public void setTechnology(String technology) {
        mTechnology = technology;
    }
    //endregion
}

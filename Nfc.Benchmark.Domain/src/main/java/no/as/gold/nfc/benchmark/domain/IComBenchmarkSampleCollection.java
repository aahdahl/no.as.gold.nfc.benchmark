package no.as.gold.nfc.benchmark.domain;

import java.util.Collection;

/**
 * This class holds a set of samples and provides a set of convenience functions to investigate and handle the collections
 * Created by aage on 13.12.13.
 */
public interface IComBenchmarkSampleCollection extends Collection<IComBenchmarkSample> {
    /**
     * Calculates the average transfer speed of the sample collection
     * @return Average transfer speed
     */
    public double calculateAverageSpeed();

    /**
     * Calculates the rate of failed transfers.
     * @return rate of failed transfers.
     */
    public double calculateTransferFailureRate();

    /**
     * Calculates the rate of transfers with communication error.
     * @return rate of transfers with communication error.
     */
    public double calculateCommunicationFailureRate();

    /**
     * Calculates the rate of successfull transfers.
     * @return Rate of successful transfers.
     */
    public double calculateSuccessRate();

    /**
     * Getter for the number of successful transfers
     * @return Number of successful transfers
     */
    public int getSuccessCount();

    /**
     * Getter for the number of failed transfers
     * @return Number of failed transfers
     */
    public int getFailCount();

    /**
     * Getter for the communication failure count
     * @return Number of times the communication has timed out.
     */
    public int getComFailCount();

    /**
     * Gets the size of the packages in this collection
     * @return size of the packages
     */
    public int getPackageSize();
}

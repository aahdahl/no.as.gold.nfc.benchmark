package no.as.gold.nfc.benchmark.domain;

import java.util.Date;

/**
 * This sample stores thr result of a single read.
 * Created by aage on 12.12.13.
 */
public interface IComBenchmarkSample {
    /**
     * Status of the transfer.
     * Succeed: successfully transferred data.
     * Fail: error in the transferred data.
     * CommunicationError: unable to transfer data.
     */
    public enum Status {
        Succeed, Fail, CommunicationError
    }

    /**
     * Gets the name of the benchmark that this sample belongs to
     * @return Name of benchmark
     */
    public String getBenchmarkName();

    /**
     * Sets the name of the benchmark that this sample belongs to
     * @param name Name of benchmark
     */
    public void setBenchmarkName(String name);

    /**
     * Gets the timestamp of the sample
     * @return
     */
    public Date getTimeStamp();

    /**
     * Sets the time stamp of the sample
     * @param timeStamp time of sample
     */
    public void setTimeStamp(Date timeStamp);

    /**
     * Gets the size of the sample
     * @return size of the sample
     */
    public int getSampleSize();

    /**
     * Sets the size of the sample
     * @param sampleSize size of the sample
     */
    public void setSampleSize(int sampleSize);

    /**
     * Gets the time taken to transfer the bytes
     * @return Time taken to transfer bytes in seconds
     */
    public double getSampleDuration();

    /**
     * Sets the time taken to transfer bytes
     * @param sampleDuration time taken to transfer bytes in seconds
     */
    public void setSampleDuration(double sampleDuration);

    /**
     * Sets the status of the transfer
     * @param status status of the transfer
     */
    public void setSampleStatus(Status status);

    /**
     * Gets the status of the communication
     * @return Status of the communication
     */
    public Status getSampleStatus();

    /**
     * Gets the technology communicated with when getting this sample
     * @return Class name of nfc technology communicated with.
     */
    public String getTechnology();

    /**
     * Sets the technology communicated with when getting this sample
     * @param technology Class name of nfc technology communicated with.
     */
    public void setTechnology(String technology);
}

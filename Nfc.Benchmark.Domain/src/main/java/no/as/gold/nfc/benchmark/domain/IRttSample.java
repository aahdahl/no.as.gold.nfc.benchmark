package no.as.gold.nfc.benchmark.domain;

import java.util.Date;

/**
 * Created by Aage Dahl on 18.03.14.
 */
public interface IRttSample {
    /**
     * Gets the name of the benchmark that this sample belongs to
     * @return Name of benchmark
     */
    public String getBenchmarkName();

    /**
     * Sets the name of the benchmark that this sample belongs to
     * @param name Name of benchmark
     */
    public void setBenchmarkName(String name);

    /**
     * Gets the timestamp of the sample
     * @return
     */
    public Date getTimeStamp();

    /**
     * Sets the time stamp of the sample
     * @param timeStamp time of sample
     */
    public void setTimeStamp(Date timeStamp);

    /**
     * Gets the technology communicated with when getting this sample
     * @return Class name of nfc technology communicated with.
     */
    public String getTechnology();

    /**
     * Sets the technology communicated with when getting this sample
     * @param technology Class name of nfc technology communicated with.
     */
    public void setTechnology(String technology);

    /**
     * Gets the RTT time measured for this technology
     * @return RTT time
     */
    public double getRttTime();

    /**
     * Sets the RTT time measured for this technology
     * @param rttTime RTT time
     */
    public void setRttTime(double rttTime);
}

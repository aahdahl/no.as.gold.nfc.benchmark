package no.as.gold.nfc.benchmark.domain;

import java.util.ArrayList;

/**
 * Simple wrapper for a collection of samples that offer some convenience functions
 * Created by aage on 13.12.13.
 */
public class DefaultComBenchmarkSampleCollection extends ArrayList<IComBenchmarkSample> implements IComBenchmarkSampleCollection {

    //region IComBenchmarkSampleCollection implementation
    @Override
    public double calculateAverageSpeed() {
        double totalTime = 0;
        double totalBytes = 0;
        for(IComBenchmarkSample sample: this) {
            totalTime += sample.getSampleDuration();
            totalBytes += sample.getSampleSize();
        }
        return totalBytes / totalTime;
    }

    @Override
    public double calculateTransferFailureRate() {
        return (double)getFailCount()/size();
    }

    @Override
    public double calculateCommunicationFailureRate() {
        return (double)getFailCount()/size();
    }

    @Override
    public double calculateSuccessRate() {
        return (double) getSuccessCount()/size();
    }

    @Override
    public int getSuccessCount() {
        int successCount = 0;
        for(IComBenchmarkSample sample: this)
            successCount += sample.getSampleStatus() == IComBenchmarkSample.Status.Succeed ? 1:0;
        return successCount;
    }

    @Override
    public int getFailCount() {
        int failureCount = 0;
        for(IComBenchmarkSample sample: this)
            failureCount += sample.getSampleStatus() == IComBenchmarkSample.Status.Fail ? 1:0;
        return failureCount;
    }

    @Override
    public int getComFailCount() {
        int comFailureCount = 0;
        for(IComBenchmarkSample sample: this)
            comFailureCount += sample.getSampleStatus() == IComBenchmarkSample.Status.CommunicationError ? 1:0;
        return comFailureCount;
    }

    @Override
    public int getPackageSize() {
        if(this.size() < 1) return -1;
        return this.get(0).getSampleSize();
    }
    //endregion IComBenchmarkSampleCollection implementation
}

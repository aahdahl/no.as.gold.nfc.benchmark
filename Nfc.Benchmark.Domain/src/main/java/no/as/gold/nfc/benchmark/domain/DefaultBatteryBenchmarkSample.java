package no.as.gold.nfc.benchmark.domain;

import java.util.Date;

/**
 * Created by Aage Dahl on 11.03.14.
 */
public class DefaultBatteryBenchmarkSample implements IBatteryBenchmarkSample {

    //region Private fields
    private final String mTechnology;
    private String mBenchmarkName;
    private final double mStartLevel;
    private final double mEndLevel;
    private final double mElapsedTime;
    private final long mNumberOfBytesTransferred;
    private Date mTimeStamp;
    //endregion Private fields

    //region Constructors
    /**
     * Instantiates a sample
     * @param technology Is the fully qualified technology class name
     * @param benchmarkName User-defined description supplied with this tests
     * @param startLevel Start level of battery
     * @param endLevel Stop level of battery
     * @param elapsedTime Time elapsed
     * @param numberOfBytesTransferred Number of bytes transferred
     */
    public DefaultBatteryBenchmarkSample(String benchmarkName, String technology, Date timeStamp, double startLevel, double endLevel, double elapsedTime, long numberOfBytesTransferred) {
        mTechnology = technology;
        mBenchmarkName = benchmarkName;
        mTimeStamp = timeStamp;
        mStartLevel = startLevel;
        mEndLevel = endLevel;
        mElapsedTime = elapsedTime;
        mNumberOfBytesTransferred = numberOfBytesTransferred;
    }
    //endregion Constructors

    //region IBatteryBenchmarkSample implementation
    @Override
    public String getBenchmarkName() {
        return mBenchmarkName;
    }

    @Override
    public void setBenchmarkName(String name) {
        mBenchmarkName = name;
    }

    @Override
    public Date getTimeStamp() {
        return mTimeStamp;
    }

    @Override
    public void setTimeStamp(Date timeStamp) {
        mTimeStamp = timeStamp;
    }

    @Override
    public String getTechnology() {
        return mTechnology;
    }

    @Override
    public String getDescription() {
        return mBenchmarkName;
    }

    @Override
    public double getStartLevel() {
        return mStartLevel;
    }

    @Override
    public double getEndLevel() {
        return mEndLevel;
    }

    @Override
    public double getElapsedTime() {
        return mElapsedTime;
    }

    @Override
    public long getNumberOfBytesTransferred() {
        return mNumberOfBytesTransferred;
    }
    //endregion IBatteryBenchmarkSample implementation
}

package no.as.gold.nfc.benchmark.controller;

import android.content.Context;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TableLayout;
import android.widget.ToggleButton;

import java.util.UUID;

import no.as.gold.nfc.benchmark.controller.engines.IComBenchmarkEngine;
import no.as.gold.nfc.benchmark.controller.engines.IBenchmarkResultHandler;
import no.as.gold.nfc.benchmark.controller.engines.NdefWriteBenchmarkEngine;
import no.as.gold.nfc.benchmark.view.R;
import no.as.gold.nfc.communication.Utils.Identifiers;
import no.as.gold.simplemessenger.MessengerService;
import no.as.gold.simplemessenger.messages.InformationMessage;

/**
 * Created by Aage Dahl on 04.02.14.
 */
public class NdefWriteBenchmarkFragment extends AbstractBenchmarkResultsFragment {

    //region Fields
    View mRootView;
    private ToggleButton mBenchmarkButton;
    private Context mContext;
    private NfcAdapter mAdapter;
    private UUID mReadUUID = UUID.fromString(Identifiers.NdefWriteBenchmarkReaderID);
    private UUID mWriteUUID = UUID.fromString(Identifiers.NdefWriteBenchmarkWriterID);
    private BenchmarkSettingsFragment mSettingsFragment = new BenchmarkSettingsFragment();
    private TableLayout mTable;
    private ToggleButton mSimulationButton;
    private View mFragmentContainer;
    //endregion

    //region AbstractBenchmarkResultsFragment implementation
    @Override
    public TableLayout getTable() {
        return mTable;
    }

    @Override
    public Context getContext() {
        return mContext;
    }
    //endregion

    //region Properties
    private void setTable(TableLayout table) {
        mTable = table;
    }

    // Fragment containing the settings
    public BenchmarkSettingsFragment getSettingsFragment() {
        return mSettingsFragment;
    }
    private void setSettingsFragment(BenchmarkSettingsFragment mSettingsFragment) {
        this.mSettingsFragment = mSettingsFragment;
    }

    public UUID getWriteUUID() {
        return mWriteUUID;
    }
    public UUID getReadUUID() {
        return mReadUUID;
    }


    //endregion

    //region Fragment overrides
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Initiate view
        mRootView = inflater.inflate(R.layout.fragment_ndef_write_benchmark,
                container, false);

        setTable((TableLayout) mRootView.findViewById(R.id.results_table_layout));
        mBenchmarkButton = (ToggleButton)mRootView.findViewById(R.id.benchmark_toggle_button);

        mContext = mRootView.getContext();

        // Get nfc adapter
        mAdapter = NfcAdapter.getDefaultAdapter(mContext);

        // Set button listener
        mBenchmarkButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            IComBenchmarkEngine engine;
            @Override
            public void onCheckedChanged(final CompoundButton compoundButton, boolean started) {
                if(started) {
                    // Clear current view
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            getTable().removeAllViews();
                        }
                    });

                    engine = new NdefWriteBenchmarkEngine(getReadUUID(), getWriteUUID());

                    // Update GUI when new sample arrives
                    engine.setSampleHandler(new IBenchmarkResultHandler() {
                        @Override
                        public void run() {
                            updateGui(engine.getSamples());
                        }
                    });

                    // Set button to off state when finished
                    engine.setBenchmarkCompletedHandler(new IBenchmarkResultHandler() {
                        @Override
                        public void run() {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    compoundButton.setChecked(false);
                                }
                            });
                        }
                    });

                    // start benchmarking
                    try {
                        engine.startBenchmarking(mSettingsFragment.GetSettings());
                    }catch (Exception e) {
                        // If error in startup -> report!
                        MessengerService.Default.send(new InformationMessage(e.getMessage()));
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                compoundButton.setChecked(false);
                            }
                        });
                    }
                } else {
                    // Stop benchmarking
                    engine.stopBenchmarking();
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            compoundButton.setChecked(false);
                        }
                    });
                }
            }
        });

        // Register message listeners
        registerMessageListeners();

        return mRootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        getFragmentManager().beginTransaction().add(R.id.ndef_write_fragment_container_layout, getSettingsFragment()).commit();
    }

    @Override
    public void onPause() {
        super.onPause();
        getFragmentManager().beginTransaction().remove(getSettingsFragment()).commit();
    }

    //endregion Fragment overrides

    //region Private methods
    private void registerMessageListeners() {
    }
    //endregion

    //region public functions

    //endregion public classes


}

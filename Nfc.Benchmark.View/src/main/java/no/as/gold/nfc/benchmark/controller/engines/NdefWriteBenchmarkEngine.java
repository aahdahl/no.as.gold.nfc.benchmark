package no.as.gold.nfc.benchmark.controller.engines;

import android.nfc.Tag;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import java.util.zip.CRC32;

import no.as.gold.nfc.benchmark.domain.DefaultComBenchmarkSample;
import no.as.gold.nfc.benchmark.domain.DefaultComBenchmarkSampleCollection;
import no.as.gold.nfc.benchmark.domain.IComBenchmarkSample;
import no.as.gold.nfc.benchmark.models.IBenchmarkSettingsModel;
import no.as.gold.nfc.benchmark.persistence.DatabaseHandler;
import no.as.gold.nfc.benchmark.utils.ObjectContainer;
import no.as.gold.nfc.communication.NfcComHandler;
import no.as.gold.nfc.communication.messages.ComHandlerMessage;
import no.as.gold.nfc.communication.technologies.WrapperFactory;
import no.as.gold.simplemessenger.MessageHandler;
import no.as.gold.simplemessenger.MessengerService;
import no.as.gold.simplemessenger.messages.InformationMessage;

/**
 * This benchmarking works by first writing a random string to the tag and then reading this back again from the tag.
 * If what is read is not what was written, a read is repeated, if the same is read again, the write is assumed to have failed.
 * If the CRC of the read is correct, the write is assumed to be correct.
 * If write fails, it is noted as communication problem.
 * Created by Aage Dahl on 09.02.14.
 */
public class NdefWriteBenchmarkEngine extends AbstractComBenchmarkEngine {
    private Object mKey = new Object();
    private static final String mName = "NDEF write benchmark";

    //region Constructors
    public NdefWriteBenchmarkEngine(UUID readUUID, UUID writeUUID) {
        super(readUUID, writeUUID);
    }
    //endregion Constructors

    //region AbstractComBenchmarkEngine implementation
    @Override
    protected void runBenchmark(final IBenchmarkSettingsModel settings) {
        // Continuous read
        MessengerService.Default.Register(mKey, ComHandlerMessage.class, new MessageHandler<ComHandlerMessage>() {
            int mInterval;
            int mStartSize;
            int mSteps;
            int mReps;
            int mMessageCount = 0;
            Tag mTag;
            public ArrayList<CRC32> readCRCs = new ArrayList<CRC32>();
            DefaultComBenchmarkSample mSample;

            /**
             * Initializing the anonymous class
             * @param settings settings to use for the test
             * @param tag mTag to communicate with
             * @return Instance of anonymous class
             */
            MessageHandler<ComHandlerMessage> initialize(IBenchmarkSettingsModel settings, Tag tag) {
                mInterval = settings.getIntervalSize();
                mStartSize = settings.getStartSize();
                mSteps = (settings.getStopSize() - settings.getStartSize())/ mInterval;
                mReps = settings.getRepetitions();
                mTag = tag;
                return this;
            }

            @Override
            public void handle(ComHandlerMessage msg) {
                if(msg.getSenderUUID().equals(mWriteUUID))
                    handleWrite(msg);
                else if(msg.getSenderUUID().equals(mReadUUID))
                    handleRead(msg);
            }

            /**
             * Handles reading, updates GUI and logs results
             * @param msg
             */
            private void handleRead(ComHandlerMessage msg) {
                if (msg.Status == ComHandlerMessage.StatusOptions.READ_SUCCESS) {
                    mSample.setSampleStatus(IComBenchmarkSample.Status.Succeed);
                    addSample();
                    writeNext();
                } else if (msg.Status == ComHandlerMessage.StatusOptions.CONNECTION_ERROR) {
                    NfcComHandler.NdefComHandler.Read(mTag, mReadUUID);
                } else if (msg.Status == ComHandlerMessage.StatusOptions.READ_FAILED) {
                    // If CRC already exist -> highly likely that write failed
                    for(CRC32 crc : readCRCs) {
                        if(crc.getValue() == msg.getCRC().getValue()) {
                            mSample.setSampleStatus(IComBenchmarkSample.Status.Fail);
                            addSample();
                            writeNext();
                            return;
                        }
                    }
                    // if not -> read again
                    readCRCs.add(msg.getCRC());
                    NfcComHandler.NdefComHandler.Read(mTag, mReadUUID);
                }
            }

            /**
             * Handles responce to writing to the mTag
             * @param msg
             */
            private void handleWrite(ComHandlerMessage msg) {
                Date timeStamp = new Date();
                double duration = msg.TransferTimeInSeconds;
                int size = msg.TransferSizeInBytes;
                mSample =  new DefaultComBenchmarkSample(settings.getName(), WrapperFactory.getTechnology(mTag).toString(), timeStamp, size, duration, null);

                if(msg.Status.equals(ComHandlerMessage.StatusOptions.WRITE_SUCCESS)) {
                    // Assert correct write by reading
                    readCRCs.clear();
                    NfcComHandler.NdefComHandler.Read(mTag, mReadUUID);
                    return;
                }
                // Writing failed...
                if(msg.Status == ComHandlerMessage.StatusOptions.CONNECTION_ERROR) {
                    mSample.setSampleStatus(IComBenchmarkSample.Status.CommunicationError);
                } else {
                    mSample.setSampleStatus(IComBenchmarkSample.Status.Fail);
                }
                addSample();
                writeNext();
            }

            /**
             * Calculates the size of the next string and transfers it.
             * Stops when reaching the end of the benchmark
             */
            private void writeNext() {
                // If finished, stop listener and run completion handle
                if((mMessageCount >= mSteps * mReps)) {
                    MessengerService.Default.UnRegister(mKey, ComHandlerMessage.class);
                    mBenchmarkCompleteHandler.run();
                    return;
                }

                // new interval -> new sample collection
                if(mMessageCount % mReps == 0) {
                    mSamples.add(new DefaultComBenchmarkSampleCollection());
                }

                int stringLength = mStartSize +  (mMessageCount*mInterval/ mReps);
                NfcComHandler.NdefComHandler.Write(mTag, mWriteUUID, RandomStringUtils.randomAlphabetic(stringLength));
            }

            /**
             * Adds a sample to the collection based on the status of msg
             */
            private void addSample() {
                // add sample to current Benchmark collection
                mMessageCount++;
                MessengerService.Default.send(new InformationMessage(String.format("Progress: %d%%", 100 * mMessageCount/(mSteps * mReps))));
                mSamples.get(mSamples.size() - 1).add(mSample);

                // Run sample handle
                mSampleHandler.run();
            }
        }.initialize(settings, ObjectContainer.Default.getTag()));

        // Initial write!
        mSamples.add(new DefaultComBenchmarkSampleCollection());
        NfcComHandler.NdefComHandler.Write(ObjectContainer.Default.getTag(), mWriteUUID, RandomStringUtils.randomAlphabetic(settings.getStartSize()));
    }
    //region AbstractComBenchmarkEngine implementation

    //region IComBenchmarkEngine implementation
    @Override
    public void stopBenchmarking() {
        MessengerService.Default.UnRegister(mKey, ComHandlerMessage.class);
        MessengerService.Default.send(new InformationMessage("Ndef write benchmark stopped"));
    }

    @Override
    public void persist(DatabaseHandler dbh) {
        throw new UnsupportedOperationException("Persistence not implemented for NdefWriteBenchmarkEngine");
    }

    @Override
    public String getName() {
        return mName;
    }
    //endregion IComBenchmarkEngine implementation

}

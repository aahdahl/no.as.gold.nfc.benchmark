package no.as.gold.nfc.benchmark.controller;

//import android.app.Fragment;
import android.content.Context;
        import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

        import java.util.UUID;

import no.as.gold.nfc.benchmark.view.R;
        import no.as.gold.nfc.communication.NfcComHandler;
import no.as.gold.nfc.communication.Utils.Identifiers;

public class WriteTagFragment extends BaseTagFragment {

    EditText mWriteEditText;
    Button mWriteButton;
    View mRootView;
    NfcAdapter mAdapter;
    Context mContext;
    private Handler mGuiHandler;
    private UUID mUUID = UUID.fromString(Identifiers.ReaderFragmentID);

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Initiate view
        mRootView = inflater.inflate(R.layout.fragment_write_tag,
                container, false);

        mWriteEditText = (EditText)mRootView.findViewById(R.id.tag_write_content_text);
        mWriteButton = (Button)mRootView.findViewById(R.id.write_tag_button);

        mContext = mRootView.getContext();

        // Get nfc adapter
        mAdapter = NfcAdapter.getDefaultAdapter(mContext);

        // Set button listener
        mWriteButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                // Write info to tag
                String text = mWriteEditText.getText().toString();

                NfcComHandler.NdefComHandler.Write(GetTag(), mUUID, text);

				/*
				// Ask for last intent from main thread
				if(lastIntent == null) return;

				if(!lastIntent.getAction().equals(NfcAdapter.ACTION_TAG_DISCOVERED)) {
					Toast.makeText(mContext, "Please bring close to readable tag to process", Toast.LENGTH_SHORT).show();
					return;
				}
				// Write info to tag
				Tag tag = lastIntent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
				if(supportedTechs(tag.getTechList())) {
					if(writableTag(tag)) {
					}
				}
				*/

            }
        });

        return mRootView;
    }

    /**
     * Makes sure that the given tech is supported - expand as we go
     * @param techList List of required technologies based on the tag to write to
     * @return True if technologies are supported, false if not.
     */
    private boolean supportedTechs(String[] techList) {
        return true;
    }

    /**
     * Checks if the tag is writable.
     * @param tag tag to check.
     * @return true if it is writable, false if not.
     */
    private boolean writableTag(Tag tag) {
        return true;
    }

}

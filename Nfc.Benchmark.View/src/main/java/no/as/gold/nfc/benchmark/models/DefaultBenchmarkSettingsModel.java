package no.as.gold.nfc.benchmark.models;

public class DefaultBenchmarkSettingsModel extends AbstractBenchmarkSettingsModel {
    private int mStartSize;
    private int mStopSize;
    private int mIntervalSize;
    private int mRepetitions;
    private int mPowerDrop;
    private String mName;

    /**
     * Initiates with the following settings:
     * Start size 0
     * End size 10
     * Interval size 1
     * Repetitions 2
     */
    public DefaultBenchmarkSettingsModel() {
        this(1, 10, 1, 2);
    }

    public DefaultBenchmarkSettingsModel(int startSize, int stopSize, int intervalSize, int repetitions) {
        try {
            setStartSize(startSize);
            setStopSize(stopSize);
            setIntervalSize(intervalSize);
            setRepetitions(repetitions);
        }catch (Exception e) {

        }
    }

    @Override
    public void setName(String name) {
        if (mName != null && mName.equals(name)) return;

        mName = name;
        notifyObservers();
    }

    @Override
    public String getName() {
        return mName;
    }

    /**
     * Gets the start size in bytes of this benchmark
     * @return start size of this benchmark
     */
    @Override
    public int getStartSize() {
        return mStartSize;
    }

    /**
     * Sets the start size of a benchmark
     * @param value Number of bytes
     * @throws Exception thrown when a negative value is set
     */
    @Override
    public void setStartSize(int value) throws Exception {
        if(value < 0) throw new Exception();
        if(mStartSize == value) return;
        mStartSize = value;
        notifyObservers();
    }

    /**
     * Gets the stop size in bytes of this benchmark
     * @return stop size of this benchmark
     */
    @Override
    public int getStopSize() {
        return mStopSize;
    }

    /**
     * Sets the stop size of a benchmark
     * @param value Number of bytes
     * @throws Exception thrown when a negative value is set
     */
    @Override
    public void setStopSize(int value) throws Exception {
        if(value < 0) throw new Exception();
        if(mStopSize == value) return;
        mStopSize = value;
        notifyObservers();
    }

    /**
     * Gets the interval size in bytes of this benchmark
     * @return start interval of this benchmark
     */
    @Override
    public int getIntervalSize() {
        return mIntervalSize;
    }

    /**
     * Sets the interval size of a benchmark
     * @param value Number of bytes
     * @throws Exception thrown when a negative value is set
     */
    @Override
    public void setIntervalSize(int value) throws Exception {
        if(value < 0) throw new Exception();
        if(mIntervalSize == value) return;
        mIntervalSize = value;
        notifyObservers();
    }

    /**
     * Gets the number of repetitions for each sample for this benchmark
     * @return Number of repetitions for each sample
     */
    @Override
    public int getRepetitions() {
        return mRepetitions;
    }

    /**
     * Sets the number of repetitions for each sample of a benchmark
     * @param value Number of bytes
     * @throws Exception thrown when a negative value is set
     */
    @Override
    public void setRepetitions(int value) throws Exception {
        if(value < 0) throw new Exception();
        if(mRepetitions == value) return;
        mRepetitions = value;
        notifyObservers();
    }

    @Override
    public void setPowerDrop(int powerDrop) {
        if(mPowerDrop == powerDrop) return;
        mPowerDrop = powerDrop;
        notifyObservers();
    }

    @Override
    public int getPowerDrop() {
        return mPowerDrop;
    }
}

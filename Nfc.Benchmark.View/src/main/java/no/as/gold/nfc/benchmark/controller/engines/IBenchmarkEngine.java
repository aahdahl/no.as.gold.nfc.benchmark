package no.as.gold.nfc.benchmark.controller.engines;

import java.io.IOException;

import no.as.gold.nfc.benchmark.models.IBenchmarkSettingsModel;
import no.as.gold.nfc.benchmark.persistence.DatabaseHandler;

/**
 * Created by Aage Dahl on 11.03.14.
 */
public interface IBenchmarkEngine<T extends IBenchmarkResultHandler> {
    /**
     * Gets the name of this engine.
     * @return Name of this engine.
     */
    String getName();

    /**
     * Starts the benchmarking
     * @throws java.lang.IllegalArgumentException on error with settings
     * @param settings Settings that define the execution of the benchmarking
     */
    void startBenchmarking(IBenchmarkSettingsModel settings) throws IllegalArgumentException, NullPointerException, IOException;

    /**
     * Interrupts the benchmarking
     */
    void stopBenchmarking();

    /**
     * Persists the samples contained in this engine to the given database.
     * @param dbh database handler
     */
    void persist(DatabaseHandler dbh) throws Exception;

/**
     * Callback handle when the benchmarking is finished and the results need to be published
     * @param handler Callback function for handling the finalization of the function
     */
    void setBenchmarkCompletedHandler(T handler);
}

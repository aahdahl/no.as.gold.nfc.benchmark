package no.as.gold.nfc.benchmark.controller;

import android.support.v4.app.Fragment;
//import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import java.util.Observable;
import java.util.Observer;

import no.as.gold.nfc.benchmark.models.AbstractBenchmarkSettingsModel;
import no.as.gold.nfc.benchmark.models.DefaultBenchmarkSettingsModel;
import no.as.gold.nfc.benchmark.models.IBenchmarkSettingsModel;
import no.as.gold.nfc.benchmark.view.R;

/**
 * This {@link Fragment} controls and gives easy access to user input.
 * Created by aage on 11.12.13.
 */
public class BenchmarkSettingsFragment extends Fragment implements Observer {

    //region Static fields
    private static EditText mStartSizeEditText;
    private static EditText mEndSizeEditText;
    private static EditText mIntervalSizeEditText;
    private static EditText mRepetitionsEditText;

    private final String START_DATA = "START";
    private final String END_DATA = "END";
    private final String INTERVAL_DATA = "INTERVAL";
    private final String REPETITIONS_DATA = "REPETITIONS";
    //endregion

    //region Fields
    private View mRootView;
    private AbstractBenchmarkSettingsModel mBenchmarkSettings;
    //endregion

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mRootView = inflater.inflate(R.layout.fragment_benchmark_settings,
            container, false);

        // Extract text fields etc.
        mStartSizeEditText = (EditText)mRootView.findViewById(R.id.start_size_edit_text);
        mEndSizeEditText = (EditText)mRootView.findViewById(R.id.end_size_edit_text);
        mIntervalSizeEditText = (EditText)mRootView.findViewById(R.id.interval_size_edit_text);
        mRepetitionsEditText = (EditText)mRootView.findViewById(R.id.repetitions_edit_text);

        bindModelToView();
        return mRootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        update(null, null);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        // store state
        savedInstanceState.putString(START_DATA, mStartSizeEditText.getText().toString());
        savedInstanceState.putString(END_DATA, mEndSizeEditText.getText().toString());
        savedInstanceState.putString(INTERVAL_DATA, mIntervalSizeEditText.getText().toString());
        savedInstanceState.putString(REPETITIONS_DATA, mRepetitionsEditText.getText().toString());

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // ---- magic lines starting here -----
        // call this to re-connect with an existing
        // loader (after screen configuration changes for e.g!)
        LoaderManager lm = getLoaderManager();
        if (lm.getLoader(0) != null) {
            lm.initLoader(0, null, (LoaderManager.LoaderCallbacks<Object>) this);
        }
        // ----- end magic lines -----

        if(savedInstanceState == null) return;
        mStartSizeEditText.setText(savedInstanceState.getString(START_DATA));
        mEndSizeEditText.setText(savedInstanceState.getString(END_DATA));
        mRepetitionsEditText.setText(savedInstanceState.getString(REPETITIONS_DATA));
        mIntervalSizeEditText.setText(savedInstanceState.getString(INTERVAL_DATA));
    }

    /**
     * Binds changes made to text elements to AbstractBenchmarkSettingsModel
     */
    private void bindModelToView() {
        if(mBenchmarkSettings == null) {
            mBenchmarkSettings = new DefaultBenchmarkSettingsModel();
            mBenchmarkSettings.addObserver(this);
        }

        // Start size binding
        mStartSizeEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    mBenchmarkSettings.setStartSize(Integer.parseInt(editable.toString()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        mEndSizeEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    mBenchmarkSettings.setStopSize(Integer.parseInt(editable.toString()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        mIntervalSizeEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    mBenchmarkSettings.setIntervalSize(Integer.parseInt(editable.toString()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        mRepetitionsEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    mBenchmarkSettings.setRepetitions(Integer.parseInt(editable.toString()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    // Updates view when model changes
    public void update(Observable observable, Object o) {
        // Pull changes from model
        mStartSizeEditText.setText(String.valueOf(mBenchmarkSettings.getStartSize()));
        mEndSizeEditText.setText(String.valueOf(mBenchmarkSettings.getStopSize()));
        mIntervalSizeEditText.setText(String.valueOf(mBenchmarkSettings.getIntervalSize()));
        mRepetitionsEditText.setText(String.valueOf(mBenchmarkSettings.getRepetitions()));
    }

    /**
     * Gets the settings set by this fragment.
     * @return The settings of a test
     */
    public IBenchmarkSettingsModel GetSettings() {
        return mBenchmarkSettings;
    }
}

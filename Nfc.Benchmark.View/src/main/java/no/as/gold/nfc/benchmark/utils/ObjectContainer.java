package no.as.gold.nfc.benchmark.utils;

import android.content.Context;
import android.nfc.NfcAdapter;
import android.nfc.Tag;

import no.as.gold.nfc.benchmark.controller.messages.NewIntentMessage;
import no.as.gold.simplemessenger.MessageHandler;
import no.as.gold.simplemessenger.MessengerService;

/**
 * This is a class that maintains references to resources to make them publicly available to any object.
 * Created by Aage Dahl on 11.03.14.
 */
public class ObjectContainer {
    public static final ObjectContainer Default = new ObjectContainer();
    private volatile Context mContext;
    private volatile Tag mTag;

    private ObjectContainer() {
        registerMessageListeners();
    }


    /**
     * Gets the context of this app.
     * @return Context of this app.
     */
    public Context getAppContext() {return mContext;}

    /**
     * Sets the context of this app.
     * @param context Context of this app.
     */
    public void setAppContext(Context context) { mContext = context; }

    public Tag getTag() {
        return mTag;
    }


    //region Private methods
    private void registerMessageListeners() {
        // Register listener for new tags
        MessengerService.Default.Register(this, NewIntentMessage.class, new MessageHandler<NewIntentMessage>() {

            @Override
            public void handle(NewIntentMessage msg) {
                Tag tag = msg.Intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
                if(tag != null) mTag = tag;
            }
        });
    }
    //endregion
}

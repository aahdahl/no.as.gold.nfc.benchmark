package no.as.gold.nfc.benchmark.models;

import java.util.Observable;

/**
 * This is a class witch implements the {@link IBenchmarkSettingsModel} interface and extends the {@link java.util.Observable} class
 * Created by aage on 11.12.13.
 */
public abstract class AbstractBenchmarkSettingsModel extends Observable implements IBenchmarkSettingsModel {
}

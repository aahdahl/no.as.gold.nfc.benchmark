package no.as.gold.nfc.benchmark.models;

import java.io.IOException;

import no.as.gold.nfc.benchmark.controller.engines.IBenchmarkEngine;
import no.as.gold.simplemessenger.MessengerService;
import no.as.gold.simplemessenger.messages.ErrorMessage;

/**
 * This class controls the lifecycle of a benchmark engine. It supplies functions and setters for benchmark settings
 * Created by aage on 13.12.13.
 */
public class DefaultBenchmarkModel extends DefaultBenchmarkSettingsModel {

    //region Private fields
    private volatile IBenchmarkEngine mEngine;
    private volatile boolean mRunning;
    //endregion Private fields

    //region Constructors
    //endregion Constructors

    /**
     * Starts async execution of benchmark. Procedure will run until complete or stopped
     * @param engine Engine to run the benchmark
     * @throws IOException If settings are not accepted
     */
    public void start(IBenchmarkEngine engine) throws IOException {
        mEngine = engine;
        // Start thread to run the execution of benchmark asynchronously
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    mEngine.startBenchmarking(DefaultBenchmarkModel.this);
                } catch (Exception e) {
                    MessengerService.Default.send(new ErrorMessage("Benchmarking interrupted: " + e.getMessage()));
                }
                // Benchmark completed
                setRunning(false);
            }
        }).start();
    }

    /**
     * Stops the benchmarking procedure
     */
    public void stop() {
        mEngine.stopBenchmarking();
    }

    /**
     * Sets the state of the benchmark
     * @param running State
     */
    private void setRunning(boolean running) {
        if(mRunning == running) return;
        mRunning = running;
        notifyObservers();
    }

    /**
     * Gets the state of the benchmark
     * @return True if running, false if not.
     */
    public boolean getRunning() {
        return mRunning;
    }
}

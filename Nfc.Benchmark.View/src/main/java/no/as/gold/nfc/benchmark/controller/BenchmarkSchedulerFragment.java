package no.as.gold.nfc.benchmark.controller;

import android.content.Context;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Observable;
import java.util.Observer;

import no.as.gold.nfc.benchmark.controller.engines.BReadAllBenchmarkEngine;
import no.as.gold.nfc.benchmark.controller.engines.BReadBenchmarkEngine;
import no.as.gold.nfc.benchmark.controller.engines.BWriteBenchmarkEngine;
import no.as.gold.nfc.benchmark.controller.engines.BatteryBenchmarkPowerEngine;
import no.as.gold.nfc.benchmark.controller.engines.BatteryBenchmarkReadEngine;
import no.as.gold.nfc.benchmark.controller.engines.BatteryBenchmarkWriteEngine;
import no.as.gold.nfc.benchmark.controller.engines.IBenchmarkEngine;
import no.as.gold.nfc.benchmark.controller.engines.RttBenchmarkEngine;
import no.as.gold.nfc.benchmark.models.SchedulerModel;
import no.as.gold.nfc.benchmark.view.R;
import no.as.gold.simplemessenger.MessengerService;
import no.as.gold.simplemessenger.messages.ErrorMessage;
import no.as.gold.simplemessenger.messages.InformationMessage;

/**
 * This fragment will control the scheduling of benchmark tests. The tests will be performed in sequence.
 * Created by Aage Dahl on 10.03.14.
 */
public class BenchmarkSchedulerFragment extends BaseTagFragment implements Observer {
    private View mRootView;
    private ToggleButton mStartButton;
    private Context mContext;
    private NfcAdapter mAdapter;

    ArrayList<CheckBox> mCheckboxes;
    private LinearLayout mSelectionContainer;

    private SchedulerModel mModel = new SchedulerModel();
    private EditText mNameEditText;
    private EditText mIntervalEditText;
    private EditText mRepetitionsEditText;
    private EditText mPowerDropEditText;
    private TextView mStatusEditText;

    public BenchmarkSchedulerFragment(ArrayList<IBenchmarkEngine> engines) {
        mModel.setEngines(engines);
        mModel.addObserver(this);
    }

    /**
     * Instantiates the BenchmarkScheduler with a set of known engines
     */
    public BenchmarkSchedulerFragment() {
        this(new ArrayList<>(Arrays.asList(
                // List of all default engines
                new BatteryBenchmarkReadEngine(),
                new BatteryBenchmarkWriteEngine(),
                new BatteryBenchmarkPowerEngine(),
                new RttBenchmarkEngine(),
                new BReadBenchmarkEngine(),
                new BWriteBenchmarkEngine(),
                new BReadAllBenchmarkEngine()
        )));
    }

    //region Fragment overrides
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Initiate view
        mRootView = inflater.inflate(R.layout.fragment_scheduler,
                container, false);

        mContext = mRootView.getContext();
        mStartButton = (ToggleButton)mRootView.findViewById(R.id.scheduler_toggle_button);
        mSelectionContainer = (LinearLayout)mRootView.findViewById(R.id.fragment_scheduler_selection_container_layout);
        mNameEditText = (EditText)mRootView.findViewById(R.id.scheduler_name_edit_text);
        mIntervalEditText = (EditText)mRootView.findViewById(R.id.scheduler_interval_edit_text);
        mRepetitionsEditText = (EditText)mRootView.findViewById(R.id.scheduler_repetitions_edit_text);
        mPowerDropEditText = (EditText)mRootView.findViewById(R.id.scheduler_power_drop_edit_text);
        mStatusEditText = (TextView)mRootView.findViewById(R.id.scheduler_status_text_view);


        // Get nfc adapter
        mAdapter = NfcAdapter.getDefaultAdapter(mContext);

        // Set button listener
        mStartButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(final CompoundButton compoundButton, boolean started) {
                try {
                    mModel.setStarted(started);
                } catch (Exception e) {
                    MessengerService.Default.send(new ErrorMessage("Error starting benchmark: " + e.getMessage()));
                }
            }
        });

        // Set listeners for the text boxes
        mNameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mModel.Settings.setName(s.toString());
            }
        });
        mPowerDropEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mModel.Settings.setPowerDrop(Integer.parseInt(s.toString()));
            }
        });
        mIntervalEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    mModel.Settings.setIntervalSize(Integer.parseInt(s.toString()));
                } catch (Exception e) {
                    MessengerService.Default.send(new InformationMessage(e.getMessage()));
                }
            }
        });
        mRepetitionsEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    mModel.Settings.setRepetitions(Integer.parseInt(s.toString()));
                } catch (Exception e) {
                    MessengerService.Default.send(new InformationMessage(e.getMessage()));
                }
            }
        });

        return mRootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        update(mModel, null);
        updateSelectionContainer();
    }

    @Override
    public void onPause() {
        super.onPause();
        mSelectionContainer.removeAllViews(); // must be removed before pausing fragment
    }
    //endregion

    private void updateSelectionContainer() {
        if(mCheckboxes == null) {
            mCheckboxes = new ArrayList<>();
            // Pair each engine with a checkbox
            for (IBenchmarkEngine engine : mModel.getEngines()) {
                CheckBox checkBox = new CheckBox(getActivity());
                checkBox.setText(engine.getName());
                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    public IBenchmarkEngine mEngine;

                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        mModel.EngineSelection.put(mEngine, isChecked);
                    }

                    public CompoundButton.OnCheckedChangeListener initialize(IBenchmarkEngine engine) {
                        mEngine = engine;
                        return this;
                    }
                }.initialize(engine));
                mCheckboxes.add(checkBox);
            }
        }

        for(CheckBox checkBox : mCheckboxes) {
            try {
                mSelectionContainer.addView(checkBox);
            } catch(Exception e) {
                MessengerService.Default.send(new InformationMessage(e.getMessage()));
            }
        }
    }

    @Override
    public void update(Observable observable, Object data) {
        // Updates according to view
        if(!(observable instanceof SchedulerModel)) return;

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // Update button status
                mStartButton.setChecked(mModel.getStarted());

                /** one-way binding
                // Update values
                mIntervalEditText.setText(String.valueOf(mModel.Settings.getIntervalSize()));
                mRepetitionsEditText.setText(String.valueOf(mModel.Settings.getRepetitions()));
                mPowerDropEditText.setText(String.valueOf(mModel.Settings.getPowerDrop()));
                mNameEditText.setText(mModel.Settings.getName());
                 **/

                // Update status
                mStatusEditText.setText(mModel.getStatus());
            }
        });
    }
}
package no.as.gold.nfc.benchmark.controller;

import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.IsoDep;
import android.nfc.tech.MifareClassic;
import android.nfc.tech.MifareUltralight;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.nfc.tech.NfcA;
import android.nfc.tech.NfcB;
import android.nfc.tech.NfcBarcode;
import android.nfc.tech.NfcF;
import android.nfc.tech.NfcV;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;

import no.as.gold.nfc.benchmark.controller.messages.NewIntentMessage;
import no.as.gold.nfc.benchmark.persistence.DatabaseHandler;
import no.as.gold.nfc.benchmark.persistence.ExportDatabaseUtility;
import no.as.gold.nfc.benchmark.utils.ObjectContainer;
import no.as.gold.nfc.benchmark.view.R;
import no.as.gold.simplemessenger.MessageHandler;
import no.as.gold.simplemessenger.MessengerService;
import no.as.gold.simplemessenger.messages.InformationMessage;

/**
 * Created by aage on 16.12.13.
 */
public class ToolsTabFragment extends BaseTagFragment {

    //region Fields
    private View mRootView;
    private Button mFormatButton;
    private TextView mSizeTV;
    private TextView mTechnologyTV;
    //endregion

    //region Fragment overrides
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Initiate view
        mRootView = inflater.inflate(R.layout.fragment_tools_tab,
                container, false);

        // Handle format tag action
        mFormatButton = (Button)mRootView.findViewById(R.id.format_button);
        mFormatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                formatTag();
            }
        });

        mSizeTV = (TextView)mRootView.findViewById(R.id.size_text_view);
        mTechnologyTV = (TextView) mRootView.findViewById(R.id.technology_text_view);

        // update view when new tag is discovered
        MessengerService.Default.Register(this, NewIntentMessage.class, new MessageHandler<NewIntentMessage>() {

            @Override
            public void handle(NewIntentMessage msg) {
                Tag tag = msg.Intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
                if(tag != null) updateView(tag);
            }
        });

        // TODO: Remove or refactor this button - database is now stored in external storage area
        Button b = (Button)mRootView.findViewById(R.id.copy_database_button);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    ExportDatabaseUtility.exportDB();
                    MessengerService.Default.send(new InformationMessage("Database copied successfully"));
                } catch (IOException e) {
                    MessengerService.Default.send(new InformationMessage("Exception:" + e.getMessage()));
                }
            }
        });

        // Button for clearing the database
        Button clearDatabaseButton = (Button)mRootView.findViewById(R.id.clear_database_button);
        clearDatabaseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseHandler db = new DatabaseHandler(ObjectContainer.Default.getAppContext());
                try {
                    db.clearDatabase();
                } catch (Exception e) {
                    MessengerService.Default.send(new InformationMessage("Error clearing database: " + e.getMessage()));
                }finally {
                    db.close();
                }
                MessengerService.Default.send(new InformationMessage("Successfully cleared database"));
            }
        });

        return mRootView;
    }

    //endregion

    //region Private methods
    /**
     * Formats the tag if it exists. Informs the user if any problem occurs
     */
    private void formatTag() {
        NdefFormatable formatable=NdefFormatable.get(GetTag());

        if (formatable != null) {
            try {
                formatable.connect();

                try {
                    formatable.format(null);
                }
                catch (Exception e) {
                    // let the user know the tag refused to format
                    MessengerService.Default.send(new InformationMessage("This tag refuses to be formatted."));
                }
            }
            catch (Exception e) {
                // let the user know the tag refused to connect
                MessengerService.Default.send(new InformationMessage("Unable to connect to the tag. Tag was not formatted."));
            }
            finally {
                try {
                    formatable.close();
                    MessengerService.Default.send(new InformationMessage("Tag was successfully formatted."));
                } catch (IOException e) {
                    e.printStackTrace();
                    MessengerService.Default.send(new InformationMessage("Error closing connection to tag. Unknown if formatting was successful!"));
                }
            }
        }
        else {
            // let the user know the tag cannot be formatted
            MessengerService.Default.send(new InformationMessage("This tag cannot be formatted"));
        }
    }

    /**
     * Updates tag information
     */
    private void updateView(Tag tag) {
        if(tag == null) {
            return;
        }

        //get technologies
        ArrayList<String> techs = new ArrayList<String>();
        String description = "";
        for(String tech : tag.getTechList()) {
            description += tech + "\n";
            techs.add(tech);
        }
        mTechnologyTV.setText(description);

        String techSizes = "";

        if(techs.contains("android.nfc.tech.NfcA")) {
            NfcA technology = NfcA.get(tag);
            if(technology != null)
                techSizes += "android.nfc.tech.NfcA " + technology.getMaxTransceiveLength() + '\n';
        }
        if(techs.contains("android.nfc.tech.NfcB")) {
            NfcB technology = NfcB.get(tag);
            if(technology != null)
                techSizes += "android.nfc.tech.NfcB " + technology.getMaxTransceiveLength() + '\n';
        }
        if(techs.contains("android.nfc.tech.NfcF")) {
            NfcF  technology = NfcF.get(tag);
            if(technology != null)
                techSizes += "android.nfc.tech.NfcF " + technology.getMaxTransceiveLength() + '\n';
        }
        if(techs.contains("android.nfc.tech.NfcV")) {
            NfcV technology = NfcV.get(tag);
            if(technology != null)
                techSizes += "android.nfc.tech.NfcV " + technology.getMaxTransceiveLength() + '\n';

        }
        if(techs.contains("android.nfc.tech.IsoDep")) {
            IsoDep technology = IsoDep.get(tag);
            if(technology != null)
                techSizes += "android.nfc.tech.IsoDep " + technology.getMaxTransceiveLength() + '\n';
        }
        if(techs.contains("android.nfc.tech.Ndef")) {
            Ndef technology = Ndef.get(tag);
            if(technology != null) {
                techSizes += technology.getType() + " " + technology.getMaxSize() + '\n';
            }
        }
        if(techs.contains("android.nfc.tech.MifareClassic")) {
            MifareClassic technology = MifareClassic.get(tag);
            if(technology != null)
                techSizes += "android.nfc.tech.MifareClassic " + technology.getSize() + '\n';
        }
        if(techs.contains("android.nfc.tech.MifareUltralight")) {
            MifareUltralight technology = MifareUltralight.get(tag);
            if(technology != null)
                techSizes += "android.nfc.tech.MifareUltralight " + technology.getMaxTransceiveLength() + '\n';
        }
        if(techs.contains("android.nfc.tech.NfcBarcode")) {
            NfcBarcode technology = NfcBarcode.get(tag);
            if(technology != null)
                techSizes += "android.nfc.tech.MifareUltralight " + '\n';
        }

        mSizeTV.setText(techSizes);


    }
    //endregion
}

package no.as.gold.nfc.benchmark.controller.engines;

import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

import no.as.gold.nfc.benchmark.domain.IComBenchmarkSampleCollection;
import no.as.gold.nfc.benchmark.models.IBenchmarkSettingsModel;
import no.as.gold.nfc.benchmark.utils.ObjectContainer;

/**
 * Convenience class that implements basic functionality of {@link IComBenchmarkEngine}
 * Created by Aage Dahl on 09.02.14.
 */
public abstract class AbstractComBenchmarkEngine implements IComBenchmarkEngine {
    protected ArrayList<IComBenchmarkSampleCollection> mSamples = new ArrayList<IComBenchmarkSampleCollection>();
    protected IBenchmarkResultHandler mSampleHandler;
    protected IBenchmarkResultHandler mBenchmarkCompleteHandler;
    protected UUID mReadUUID;
    protected UUID mWriteUUID;

    //region IBenchmark implementation
    /**
     * Instantiates the runBenchmark with unique read and write {@link java.util.UUID}.
     * @param readUUID Used to distinguish read messages
     * @param writeUUID Used to distinguish write messages
     * @throws java.lang.IllegalArgumentException if readUUID or writeUUID is null.
     */
    protected AbstractComBenchmarkEngine(UUID readUUID, UUID writeUUID) throws IllegalArgumentException
    {
        if(readUUID == null)
            throw new IllegalArgumentException("readUUID");
        else if (writeUUID == null)
            throw new IllegalArgumentException("writeUUID");

        mReadUUID = readUUID;
        mWriteUUID = writeUUID;
    }

    public void startBenchmarking(IBenchmarkSettingsModel settings) throws IllegalArgumentException, NullPointerException, IOException {
        // Assert settings
        if(settings.getIntervalSize() < 1 || settings.getRepetitions() < 0 || settings.getStartSize() < 1 || settings.getStopSize() <= settings.getStartSize())
            throw new IllegalArgumentException("settings");

        // Clear existing samples
        getSamples().clear();

        runBenchmark(settings);

    }

    @Override
    public void setBenchmarkCompletedHandler(IBenchmarkResultHandler handler) {
        mBenchmarkCompleteHandler = handler;
    }

    @Override
    public void setSampleHandler(IBenchmarkResultHandler handler) {
        mSampleHandler = handler;
    }

    @Override
    public ArrayList<IComBenchmarkSampleCollection> getSamples() {
        return mSamples;
    }

    //endregion IBenchmark implementation

    //region Abstract methods
    /**
     * Function to be implemented by extending class. This executes the benchmarking process.
     * @param settings Settings for the benchmark
     */
    protected abstract void runBenchmark(IBenchmarkSettingsModel settings) throws IOException;
    //endregion Abstract methods


}

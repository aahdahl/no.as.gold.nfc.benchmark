package no.as.gold.nfc.benchmark.controller.engines;

import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.Tag;
import android.os.BatteryManager;

import java.util.Date;

import no.as.gold.nfc.benchmark.domain.DefaultBatteryBenchmarkSample;
import no.as.gold.nfc.benchmark.domain.IBatteryBenchmarkSample;
import no.as.gold.nfc.benchmark.models.IBenchmarkSettingsModel;
import no.as.gold.nfc.benchmark.persistence.DatabaseHandler;
import no.as.gold.nfc.benchmark.utils.ObjectContainer;
import no.as.gold.nfc.communication.Utils.Utils;
import no.as.gold.nfc.communication.technologies.ITagTechnologyWrapper;
import no.as.gold.nfc.communication.technologies.WrapperFactory;
import no.as.gold.simplemessenger.MessengerService;
import no.as.gold.simplemessenger.messages.ErrorMessage;
import no.as.gold.simplemessenger.messages.InformationMessage;

/**
 * This engine performs continuous reading from the tag until the battery level is reduced by 10%.
 * Created by Aage Dahl on 11.03.14.
 */
public class BatteryBenchmarkReadEngine implements IBenchmarkEngine {
    public IBatteryBenchmarkSample Result;
    private boolean stopped;
    private IBenchmarkResultHandler mHandler;

    //region IBenchmarkEngine implementation
    @Override
    public String getName() {
        return "Battery benchmark read";
    }

    @Override
    public void startBenchmarking(IBenchmarkSettingsModel settings) throws IllegalArgumentException, NullPointerException {
        stopped = false;
        Tag tag = ObjectContainer.Default.getTag();

        // Wait until charger is unplugged
        waitForNotCharging();

        // wait until battery level has changed (to get a fresh start)
        waitForBatteryLevelChanged();
        long nrOfBytesTransferred = 0;
        double startLevel = getBatteryLevel();
        double powerDrop = (double)settings.getPowerDrop()/100;
        MessengerService.Default.send(new InformationMessage("Starting " + getName()));

        // Register new timer
        double elapsedTime = 0;
        Utils.SimpleTimer.startTimer(getName());
        // Continue scanning - double loop to enable reconnecting if connection lost...
        while (startLevel - getBatteryLevel() < powerDrop && !stopped) {
            ITagTechnologyWrapper wrapper = null;
            try {
                // Get the last registered tag
                tag = ObjectContainer.Default.getTag();
                // Consume battery until battery level changes by x%
                wrapper = WrapperFactory.getWrapper(tag);
                wrapper.connect(WrapperFactory.getTechnology(tag));
                while (startLevel - getBatteryLevel() < powerDrop && !stopped) {
                    nrOfBytesTransferred += wrapper.readAll().length;
                }
            } catch (Exception e) {
                MessengerService.Default.send(new ErrorMessage("Error running " + getName()));
                e.printStackTrace();
                try {   // need to enable reconnecting if necessary
                    Thread.sleep(10);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            } finally {
                try {
                    if(wrapper != null)
                        wrapper.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        // Get elapsed time
        elapsedTime = Utils.SimpleTimer.getElapsedSeconds(getName());
        // Unregister timer
        Utils.SimpleTimer.removeTimer(getName());

        // Make sample
        double endLevel = getBatteryLevel();
        Result = new DefaultBatteryBenchmarkSample(settings.getName(),
                WrapperFactory.getTechnology(tag).getClass().toString(), new Date(), startLevel,
                endLevel, elapsedTime, nrOfBytesTransferred);

        MessengerService.Default.send(new InformationMessage("Finished benchmark in " + elapsedTime + " seconds"));

        // run completion handler
        if(mHandler != null) mHandler.run();
    }

    private void waitForBatteryLevelChanged() {
        // Consume battery until battery level changes to get a valid starting point
        double batteryLevel = getBatteryLevel();
        while(batteryLevel <= getBatteryLevel() && !stopped) {
            //do something...
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            MessengerService.Default.send(new InformationMessage("Waiting for battery level to drop " + getName()));
        }
    }

    @Override
    public void stopBenchmarking() {
        stopped = true;
    }

    @Override
    public void persist(DatabaseHandler dbh) throws Exception {
        dbh.addReadBatterySample(Result);
    }

    @Override
    public void setBenchmarkCompletedHandler(IBenchmarkResultHandler handler) {
        mHandler = handler;
    }
    //endregion IBenchmarkEngine implementation

    //region private methods

    /**
     * Waits until the charger is not connected
     */
    private void waitForNotCharging() {
        // Make sure it is not charging
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = ObjectContainer.Default.getAppContext().registerReceiver(null, ifilter);

        int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        while (status == BatteryManager.BATTERY_STATUS_CHARGING) {
            try {
                MessengerService.Default.send(new InformationMessage("Need to unplug the charger before running " + getName()));
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (stopped) return;
        }
    }


    /**
     * Gets the current battery level
     * @return
     */
    private double getBatteryLevel() {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = ObjectContainer.Default.getAppContext().registerReceiver(null, ifilter);
        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

        double batteryPct = level / (double)scale;
        MessengerService.Default.send(new InformationMessage("Battery level: " + batteryPct + "%"));
        return batteryPct;
    }
    //endregion private methods
}

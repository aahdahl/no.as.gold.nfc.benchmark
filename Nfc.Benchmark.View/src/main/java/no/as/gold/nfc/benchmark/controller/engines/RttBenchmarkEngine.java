package no.as.gold.nfc.benchmark.controller.engines;

import android.nfc.Tag;
import android.nfc.tech.TagTechnology;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import no.as.gold.nfc.benchmark.domain.DefaultRttSample;
import no.as.gold.nfc.benchmark.domain.IRttSample;
import no.as.gold.nfc.benchmark.models.IBenchmarkSettingsModel;
import no.as.gold.nfc.benchmark.persistence.DatabaseHandler;
import no.as.gold.nfc.benchmark.utils.ObjectContainer;
import no.as.gold.nfc.communication.technologies.WrapperFactory;
import no.as.gold.simplemessenger.MessengerService;
import no.as.gold.simplemessenger.messages.ErrorMessage;

/**
 * Created by Aage Dahl on 20.03.14.
 */
public class RttBenchmarkEngine implements IBenchmarkEngine {
    private ArrayList<IRttSample> mSamples;
    private boolean mRunning = false;
    private IBenchmarkResultHandler mHandler;

    @Override
    public String getName() {
        return "RTT Benchmark";
    }

    @Override
    public void startBenchmarking(IBenchmarkSettingsModel settings) throws IllegalArgumentException, NullPointerException {
        // perform benchmarking...
        mSamples = new ArrayList<>(settings.getRepetitions());

        // get tag from object container
        Tag currentTag = ObjectContainer.Default.getTag();
        TagTechnology tech = WrapperFactory.getTechnology(ObjectContainer.Default.getTag());
        mRunning = true;
        for(int i = 0; i < settings.getRepetitions() && mRunning; i++) {
            try {
                IRttSample sample = new DefaultRttSample();
                sample.setRttTime(WrapperFactory.getWrapper(ObjectContainer.Default.getTag()).timeRTT(tech));
                sample.setBenchmarkName(settings.getName());
                sample.setTechnology(tech.getClass().getName());
                sample.setTimeStamp(new Date());
                mSamples.add(sample);
            } catch (IOException e) {
                MessengerService.Default.send(new ErrorMessage("Unable to get RTT for this tag: " + e.getMessage()));
                e.printStackTrace();
            }
        }
        mRunning = false;

        // run completion handler
        if(mHandler != null) mHandler.run();
    }

    @Override
    public void stopBenchmarking() {
        // Stop benchmarking
        mRunning = false;
    }

    @Override
    public void persist(DatabaseHandler dbh) throws Exception {
        if(mSamples == null || mSamples.size() < 1) return;

        for(IRttSample sample : mSamples)
            dbh.addRTTSample(sample);
    }

    @Override
    public void setBenchmarkCompletedHandler(IBenchmarkResultHandler handler) {
        mHandler = handler;
    }
}

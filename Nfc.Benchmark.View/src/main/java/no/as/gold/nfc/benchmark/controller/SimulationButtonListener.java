package no.as.gold.nfc.benchmark.controller;

import android.widget.CompoundButton;

import java.util.ArrayList;
import java.util.Date;

import no.as.gold.nfc.benchmark.domain.DefaultComBenchmarkSample;
import no.as.gold.nfc.benchmark.domain.DefaultComBenchmarkSampleCollection;
import no.as.gold.nfc.benchmark.domain.IComBenchmarkSample;
import no.as.gold.nfc.benchmark.domain.IComBenchmarkSampleCollection;
import no.as.gold.nfc.benchmark.models.IBenchmarkSettingsModel;

/**
 * Adds a random sample for each press
 */
public class SimulationButtonListener implements CompoundButton.OnCheckedChangeListener {
    private final NdefReadBenchmarkFragment mNdefReadBenchmarkFragment;
    ArrayList<IComBenchmarkSampleCollection> samples = new ArrayList<IComBenchmarkSampleCollection>();
    IBenchmarkSettingsModel settings;

    public SimulationButtonListener(NdefReadBenchmarkFragment ndefReadBenchmarkFragment) {
        mNdefReadBenchmarkFragment = ndefReadBenchmarkFragment;
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        // if started
        if (b) {
            // Do in UI thread
            mNdefReadBenchmarkFragment.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mNdefReadBenchmarkFragment.getTable().removeAllViews();
                }
            });
            // Get settings
            settings = mNdefReadBenchmarkFragment.getSettingsFragment().GetSettings();
            final int interval = settings.getIntervalSize();
            final int startSize = settings.getStartSize();
            final int steps = (settings.getStopSize() - settings.getStartSize()) / interval;
            final int reps = settings.getRepetitions();

            // Loop through according to settings and fill inn with random values
            for (int i = 0; i < steps; i++) {
                IComBenchmarkSampleCollection collection = new DefaultComBenchmarkSampleCollection();
                samples.add(collection);
                int size = startSize + interval * i;
                for (int j = 0; j < reps; j++) {
                    collection.add(new DefaultComBenchmarkSample("BenchmarkName", "Test technology", new Date(), size, 100, IComBenchmarkSample.Status.Succeed));
                    mNdefReadBenchmarkFragment.updateGui(samples);
                }
            }

            // Do in UI thread
            mNdefReadBenchmarkFragment.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mNdefReadBenchmarkFragment.getSimulationButton().setChecked(false);
                    samples.clear();
                }
            });
            return;
        }

        // if stopped..
        samples.clear();

    }
}

package no.as.gold.nfc.benchmark.controller.messages;

import no.as.gold.simplemessenger.messages.IMessage;

/**
 * Created by aage on 16.10.13.
 */
public class ComStatUpdateMessage implements IMessage {

    //region Fields
    private int mReadCount;
    private int mReadFailCount;
    private int mWriteCount;
    private int mWriteFailCount;
    //endregion

    //region Properties
    public int getReadCount() { return mReadCount; }
    public int getReadFailCount() { return mReadFailCount; }
    public int getWriteCount() { return mWriteCount; }
    public int getWriteFailCount() { return mWriteFailCount; }
    //endregion

    //region Constructors
    /**
     * Sets the statistics-update contained in this message
     * @param sendCount Total number of send operations executed
     * @param sendFailCount Total number of send operation executed that failed
     * @param writeCount Total number of write operations executed
     * @param writeFailCount Total number of write operations executed that failed
     */
    public ComStatUpdateMessage(int sendCount, int sendFailCount, int writeCount, int writeFailCount) {
        mReadCount = sendCount;
        mReadFailCount = sendFailCount;
        mWriteCount = writeCount;
        mWriteFailCount = writeFailCount;
    }
    //endregion

    //region IMessage implementation
    @Override
    public String getMessage() {
        return null;
    }
    //endregion
}

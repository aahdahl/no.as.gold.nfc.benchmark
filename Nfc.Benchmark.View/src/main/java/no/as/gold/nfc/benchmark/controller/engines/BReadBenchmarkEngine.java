package no.as.gold.nfc.benchmark.controller.engines;

import android.nfc.tech.TagTechnology;

import org.apache.commons.lang3.ArrayUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

import no.as.gold.nfc.benchmark.domain.DefaultComBenchmarkSample;
import no.as.gold.nfc.benchmark.domain.DefaultComBenchmarkSampleCollection;
import no.as.gold.nfc.benchmark.domain.IComBenchmarkSample;
import no.as.gold.nfc.benchmark.domain.IComBenchmarkSampleCollection;
import no.as.gold.nfc.benchmark.models.IBenchmarkSettingsModel;
import no.as.gold.nfc.benchmark.persistence.DatabaseHandler;
import no.as.gold.nfc.benchmark.utils.ObjectContainer;
import no.as.gold.nfc.communication.Utils.Identifiers;
import no.as.gold.nfc.communication.Utils.Utils;
import no.as.gold.nfc.communication.technologies.ITagTechnologyWrapper;
import no.as.gold.nfc.communication.technologies.WrapperFactory;
import no.as.gold.simplemessenger.MessengerService;
import no.as.gold.simplemessenger.messages.ErrorMessage;
import no.as.gold.simplemessenger.messages.InformationMessage;

/**
 * Created by Aage Dahl on 03.03.14.
 */
public class BReadBenchmarkEngine extends AbstractComBenchmarkEngine {

    boolean mBenchmarkStopped = true;
    private static final String mName = "Binary read benchmark";

    /**
     * Instantiates the runBenchmark with unique read and write {@link java.util.UUID}.
     *
     * @param readUUID  Used to distinguish read messages
     * @param writeUUID Used to distinguish write messages
     * @throws IllegalArgumentException if readUUID or writeUUID is null.
     */
    public BReadBenchmarkEngine(UUID readUUID, UUID writeUUID) throws IllegalArgumentException {
        super(readUUID, writeUUID);
    }

    /**
     * Instantiates the binary benchmark engine with default R/W UUID's
     */
    public BReadBenchmarkEngine() { this(UUID.fromString(Identifiers.BReadBenchmarkReaderID), UUID.fromString(Identifiers.BReadBenchmarkWriterID));}

    @Override
    protected void runBenchmark(IBenchmarkSettingsModel settings) throws IOException {
        ITagTechnologyWrapper wrapper = null;
        byte[] data = new byte[0];

        int intervals = 0;

        // fill tag with bogus data - try up to 100 times before giving up!
        for(int i = 0; i <= 100; i++) {
            try {
                wrapper = WrapperFactory.getWrapper(ObjectContainer.Default.getTag());
                int minAddr = wrapper.getMinContiguousRWAddress();
                int maxAddr = wrapper.getMaxContiguousRWAddress();
                data = makeRandomMessage(maxAddr - minAddr);
                intervals = (maxAddr - minAddr) / settings.getIntervalSize();
                wrapper.connect(WrapperFactory.getTechnology(ObjectContainer.Default.getTag()));
                wrapper.write(data, minAddr);
                break;
            } catch (Exception e) {
                e.printStackTrace();
                MessengerService.Default.send(new ErrorMessage("Error in BReadBenchmarkEngine: " + e.getMessage()));
                try {
                    Thread.sleep(100,0);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            } finally {
                try {
                    if(wrapper != null)
                        wrapper.close();
                } catch (IOException e) {
                    MessengerService.Default.send(new ErrorMessage(e.getMessage()));
                }
            }
            if(i == 100) throw new IOException("Unable to start BReadBenchmarkEngine");
        }

        // Read intervals
        mBenchmarkStopped = false;
        String uuid = UUID.randomUUID().toString();
        for(int i = 0; i < intervals; i++) {
            int size = settings.getIntervalSize() * (i+1);
            IComBenchmarkSampleCollection samples = new DefaultComBenchmarkSampleCollection();
            getSamples().add(samples);
            // repeated readings
            for(int j = 0; j < settings.getRepetitions(); j++) {
                // Return if benchmark is stopped
                if(mBenchmarkStopped) return;

                TagTechnology tech = WrapperFactory.getTechnology(ObjectContainer.Default.getTag());
                try {
                    wrapper.connect(tech);
                    Utils.SimpleTimer.startTimer(uuid);
                    byte[] read_data = wrapper.read(size, wrapper.getMinContiguousRWAddress());
                    if (Arrays.equals(read_data, ArrayUtils.subarray(data, 0, size))) {
                        // succeeded
                        double duration = Utils.SimpleTimer.getElapsedSeconds(uuid);
                        samples.add(new DefaultComBenchmarkSample(settings.getName(), tech.getClass().getName(), new Date(), size, duration, IComBenchmarkSample.Status.Succeed));
                    } else {
                        // failed
                        double duration = Utils.SimpleTimer.getElapsedSeconds(uuid);
                        samples.add(new DefaultComBenchmarkSample(settings.getName(), tech.getClass().getName(), new Date(), size, duration, IComBenchmarkSample.Status.Fail));
                    }
                } catch (Exception e) {
                    double duration = Utils.SimpleTimer.getElapsedSeconds(uuid);
                    samples.add(new DefaultComBenchmarkSample(settings.getName(), tech.getClass().getName(), new Date(), size, duration, IComBenchmarkSample.Status.CommunicationError));
                    e.printStackTrace();
                    MessengerService.Default.send(new ErrorMessage("Error in BReadBenchmarkEngine"));
                } finally {
                    try {
                        wrapper.close();
                    } catch (IOException e) {
                        MessengerService.Default.send(new ErrorMessage(e.getMessage()));
                    }
                }
                // Inform listeners of newly arrived sample
                if(mSampleHandler != null) mSampleHandler.run();
                MessengerService.Default.send(new InformationMessage(String.format("BRead progress: interval %d/%d, repetition %d/%d", i, intervals, j, settings.getRepetitions())));
            }
        }

        if(mBenchmarkCompleteHandler != null) mBenchmarkCompleteHandler.run();
        mBenchmarkStopped = true;
    }

    @Override
    public void stopBenchmarking() {
        mBenchmarkStopped = true;
    }

    @Override
    public void persist(DatabaseHandler dbh) throws Exception {
        if(getSamples() == null) return;
        for(IComBenchmarkSampleCollection collection : this.getSamples()) {
            for(IComBenchmarkSample sample : collection) {
                dbh.addReadCommunicationSample(sample);
            }
        }
    }

    @Override
    public String getName() {
        return mName;
    }

    /**
     * Creates a random byte array
     * @param size Size of byte array
     * @return Randomized byte array
     */
    private byte[] makeRandomMessage(int size) {
        byte[] bytes = new byte[size];
        new Random(new Date().getTime()).nextBytes(bytes);
        return bytes;
    }
}

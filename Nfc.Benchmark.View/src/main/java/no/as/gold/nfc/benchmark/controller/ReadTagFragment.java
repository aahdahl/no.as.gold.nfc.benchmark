package no.as.gold.nfc.benchmark.controller;


//import android.app.Fragment;
import android.content.Context;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.UUID;

import no.as.gold.nfc.benchmark.view.R;
import no.as.gold.nfc.communication.NfcComHandler;
import no.as.gold.nfc.communication.Utils.Identifiers;
import no.as.gold.nfc.communication.messages.ComHandlerMessage;
import no.as.gold.simplemessenger.MessageHandler;
import no.as.gold.simplemessenger.MessengerService;

public class ReadTagFragment extends BaseTagFragment {

    //region Fields
    private TextView mReadTextView;
    private Button mReadButton;
    private View mRootView;
    private NfcAdapter mAdapter;
    private Context mContext;
    private ComHandlerMessage mComHandlerMessage;
    private Handler mGuiHandler;
    private UUID mUUID = UUID.fromString(Identifiers.WriterFragmentID);
    //endregion

    //region properties

    //endregion

    //region Fragment overrides
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Initiate view
        mRootView = inflater.inflate(R.layout.fragment_read_tag,
                container, false);

        mReadTextView = (TextView)mRootView.findViewById(R.id.tag_read_content_text);
        mReadButton = (Button)mRootView.findViewById(R.id.read_tag_button);

        mContext = mRootView.getContext();

        // Get nfc adapter
        mAdapter = NfcAdapter.getDefaultAdapter(mContext);

        // Set button listener
        mReadButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // Read tag content and output it to _readTextView
                readTag();
            }
        });

        // Register listeners for incoming ComHandlerMessages
        MessengerService.Default.Register(this, ComHandlerMessage.class, new MessageHandler<ComHandlerMessage>() {
            @Override
            public void handle(ComHandlerMessage message) {
                // Only listen to the messages sent by me!
                if(mUUID.equals(message.getSenderUUID())) {
                    mComHandlerMessage = message;
                    mGuiHandler.sendEmptyMessage(0);
                }
            }
        });

        // Delegate GUI changes to GUI thread
        mGuiHandler = new Handler(new GuiInteractionHandlerCallback());

        return mRootView;
    }

    //endregion

    //region Public methods
    //endregion

    //region Private methods
    /**
     * Handles reading from Tags
     */
    private void readTag() {
        // Read info on tag
        NfcComHandler.NdefComHandler.Read(GetTag(), mUUID);
    }
    //endregion

    /**
     * Inner class for delegating GUI changes to the owner thread (GUI thread)
     * @author Aage Dahl
     *
     */
    private class GuiInteractionHandlerCallback implements Handler.Callback {
        @Override
        public boolean handleMessage(Message msg) {
            // Lazy method of updating GUI
            switch (mComHandlerMessage.Status) {
                case READ_SUCCESS:
                    mReadTextView.setText(mComHandlerMessage.getReadMessage());
                    break;
                case READ_FAILED:
                    mReadTextView.setText("Failed reading from tag");
                    break;
                case CONNECTION_ERROR:
                    mReadTextView.setText("Connection error");
                    break;
                default: // not to be handled here
            }
            return true;
        }

    }
}

package no.as.gold.nfc.benchmark.models;

/**
 * Created by aage on 12.12.13.
 */
public interface IBenchmarkSettingsModel {
    //region Shared settings

    /**
     * Sets the name of this benchmark. This should enable you to distinguish one benchmark test from another.
     * @param name Name of the benchmark (eg: 10cm offset)
     */
    public void setName(String name);

    /**
     * Gets the name of this benchmark.
     * @return Name of this benchmark.
     */
    public String getName();
    //endregion Shared settings

    //region Settings for communication benchmark
    /**
     * Gets the start size in bytes of this benchmark
     *
     * @return start size of this benchmark
     */
    public int getStartSize();

    /**
     * Sets the start size of a benchmark
     *
     * @param value Number of bytes
     * @throws Exception thrown when a negative value is set
     */
    public void setStartSize(int value) throws Exception;

    /**
     * Gets the stop size in bytes of this benchmark
     *
     * @return stop size of this benchmark
     */
    public int getStopSize();

    /**
     * Sets the stop size of a benchmark
     *
     * @param value Number of bytes
     * @throws Exception thrown when a negative value is set
     */
    public void setStopSize(int value) throws Exception;

    /**
     * Gets the interval size in bytes of this benchmark
     *
     * @return start interval of this benchmark
     */
    public int getIntervalSize();

    /**
     * Sets the interval size of a benchmark
     *
     * @param value Number of bytes
     * @throws Exception thrown when a negative value is set
     */
    public void setIntervalSize(int value) throws Exception;

    /**
     * Gets the number of repetitions for each sample for this benchmark
     *
     * @return Number of repetitions for each sample
     */
    public int getRepetitions();

    /**
     * Sets the number of repetitions for each sample of a benchmark
     *
     * @param value Number of bytes
     * @throws Exception thrown when a negative value is set
     */
    public void setRepetitions(int value) throws Exception;
    //endregion Settings for communication benchmark

    //region Settings for battery benchmark

    /**
     * Sets the percentage of power to drop when estimating power performance
     * @param powerDrop Power to drop in percent
     */
    public void setPowerDrop(int powerDrop);

    /**
     * Gets the percentage of power to drop when estimating power performance
     * @return Power to drop in percent
     */
    public int getPowerDrop();
    //endregion
}

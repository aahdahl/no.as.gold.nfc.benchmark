package no.as.gold.nfc.benchmark.controller;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;

import no.as.gold.nfc.benchmark.domain.IComBenchmarkSampleCollection;
import no.as.gold.nfc.benchmark.view.R;

/**
 * This is a abstract class containing a set of functions required for all benchmarking fragments that want to display results for success, com. fail, crc err., size and speed
 * This implementation is made abstract so that any fragment with a available table for filling in results can be used
 * Created by Aage Dahl on 04.02.14.
 */
public abstract class AbstractBenchmarkResultsFragment extends BaseTagFragment {

    private int mCurrentRow = 0;

    /**
     * Very important to implement with the table where results should be published
     * @return table where results from benchmarking will be published
     */
    public abstract TableLayout getTable();

    /**
     * Gets the context to use
     * @return Context
     */
    public abstract Context getContext();
    //region public functions

    /**
     * Updates GUI based on newly added samples
     */
    public void updateGui(final ArrayList<IComBenchmarkSampleCollection> samples) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // Clear table
                TableLayout table = getTable();
                table.removeAllViews();
                addHeaders();
                for(int row = 0; row < samples.size()-1; row++) {
                    addRowToTable(samples.get(row));
                }

/*
                IComBenchmarkSampleCollection s;
                if(mCurrentRow < samples.size()) {
                    mCurrentRow++;
                    s = samples.get(mCurrentRow - 1);
                    addRowToTable(s, samples.size() - 1);
                } else {
                    s = samples.get(mCurrentRow - 1);
                }
                // Get row to edit
                TableRow row = (TableRow) getTable().findViewWithTag(s);
                // Get tv's to edit
                TextView success = (TextView)row.getTag(R.id.read_success_text_view);
                TextView fail = (TextView)row.getTag(R.id.read_fail_text_view);
                TextView connectionProblem = (TextView)row.getTag(R.id.com_error_text_view);
                TextView size = (TextView)row.getTag(R.id.package_size_text_view);
                TextView speed = (TextView)row.getTag(R.id.read_speed_text_view);
                // Update sample
                success.setText(Integer.toString(s.getSuccessCount()));
                fail.setText(Integer.toString(s.getFailCount()));
                connectionProblem.setText(Integer.toString(s.getComFailCount()));
                size.setText(Integer.toString(s.getPackageSize()));
                speed.setText(Double.toString(s.calculateAverageSpeed()));
*/
            }

            private void addRowToTable(IComBenchmarkSampleCollection s) {
                TableRow row = new TableRow(getContext());
                // add text views
                TextView successTv = new TextView(getContext());
                TextView failTv = new TextView(getContext());
                TextView connectionTv = new TextView(getContext());
                TextView speedTv = new TextView(getContext());
                TextView sizeTv = new TextView(getContext());
                successTv.setText(Integer.toString(s.getSuccessCount()));
                failTv.setText(Integer.toString(s.getFailCount()));
                connectionTv.setText(Integer.toString(s.getComFailCount()));
                sizeTv.setText(Integer.toString(s.getPackageSize()));
                speedTv.setText(Double.toString(s.calculateAverageSpeed()));
                // add id to views
                row.setTag(R.id.read_success_text_view, successTv);
                row.setTag(R.id.read_fail_text_view, failTv);
                row.setTag(R.id.com_error_text_view, connectionTv);
                row.setTag(R.id.read_speed_text_view, speedTv);
                row.setTag(R.id.package_size_text_view, sizeTv);
                // add views to row
                row.addView(successTv);
                row.addView(failTv);
                row.addView(connectionTv);
                row.addView(sizeTv);
                row.addView(speedTv);
                getTable().addView(row, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            }

            /*
            private void addRowToTable(Object tag, int rowNr) {
                if(rowNr == 0)
                    addHeaders();

                TableRow row = new TableRow(getContext());
                // set id to be index in list of collections
                row.setTag(tag);
                // add text views
                TextView successTv = new TextView(getContext());
                TextView failTv = new TextView(getContext());
                TextView connectionTv = new TextView(getContext());
                TextView speedTv = new TextView(getContext());
                TextView sizeTv = new TextView(getContext());
                // add id to views
                row.setTag(R.id.read_success_text_view, successTv);
                row.setTag(R.id.read_fail_text_view, failTv);
                row.setTag(R.id.com_error_text_view, connectionTv);
                row.setTag(R.id.read_speed_text_view, speedTv);
                row.setTag(R.id.package_size_text_view, sizeTv);
                // add views to row
                row.addView(successTv);
                row.addView(failTv);
                row.addView(connectionTv);
                row.addView(sizeTv);
                row.addView(speedTv);
                getTable().addView(row, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            }
*/
            private void addHeaders() {
                TextView successHeader = new TextView(getContext()); successHeader.setPadding(0, 0, 10, 0); successHeader.setLines(2);
                TextView failHeader = new TextView(getContext()); failHeader.setPadding(0, 0, 10, 0); failHeader.setLines(2);
                TextView connectionHeader = new TextView(getContext()); connectionHeader.setPadding(0, 0, 10, 0); connectionHeader.setLines(2);
                TextView sizeHeader = new TextView(getContext()); sizeHeader.setPadding(0,0,10,0); sizeHeader.setLines(2);
                TextView speedHeader = new TextView(getContext()); speedHeader.setPadding(0,0,10,0); speedHeader.setLines(2);
                successHeader.setText(R.string.success_text);
                failHeader.setText(R.string.fail_text);
                connectionHeader.setText(R.string.com_fail_text);
                sizeHeader.setText(R.string.package_size_text);
                speedHeader.setText(R.string.speed_text);
                TableRow row = new TableRow(getContext());
                row.addView(successHeader);
                row.addView(failHeader);
                row.addView(connectionHeader);
                row.addView(sizeHeader);
                row.addView(speedHeader);
                getTable().addView(row, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            }
        });
    }
}

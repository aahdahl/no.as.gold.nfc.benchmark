package no.as.gold.nfc.benchmark.controller.engines;

/**
 * Created by Aage Dahl on 07.02.14.
 */
public interface IBenchmarkResultHandler {
    void run();
}

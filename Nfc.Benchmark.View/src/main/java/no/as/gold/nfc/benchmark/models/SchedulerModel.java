package no.as.gold.nfc.benchmark.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Observable;
import java.util.Set;

import no.as.gold.nfc.benchmark.controller.engines.IBenchmarkEngine;
import no.as.gold.nfc.benchmark.persistence.DatabaseHandler;
import no.as.gold.nfc.benchmark.utils.ObjectContainer;
import no.as.gold.simplemessenger.MessengerService;
import no.as.gold.simplemessenger.messages.ErrorMessage;
import no.as.gold.simplemessenger.messages.InformationMessage;

/**
 * This is a model of the scheduler fragment. It contains all information stored in the view.
 * Created by Aage Dahl on 21/03/14.
 */
public class SchedulerModel extends Observable {

    //region Private fields
    // Using LinkedHashMap to preserve input order!
    public LinkedHashMap<IBenchmarkEngine, Boolean> EngineSelection = new LinkedHashMap<>();
    private boolean mRunning;
    private String mStatus;
    private IBenchmarkEngine mRunningEngine;
    //endregion Private fields

    //region Properties

    /**
     * Sets the current status of the benchmarking execution
     * @param status Status of execution
     */
    private void setStatus(String status) {
        if(mStatus != null && mStatus.equals(status)) return;
        mStatus = status;
        setChanged();
        notifyObservers();
    }

    /**
     * Gets the status message explaining the status of the execution of the benchmarks.
     * @return Message describing execution status.
     */
    public String getStatus() {
        return mStatus;
    }

    /**
     * Getter for engines available for this model
     * @return Engines available for this model
     */
    public Set<IBenchmarkEngine> getEngines() { return EngineSelection.keySet(); }
    public void setEngines(ArrayList<IBenchmarkEngine> engines) {
        EngineSelection.clear();
        for(IBenchmarkEngine engine : engines)
            EngineSelection.put(engine, false);
    }

    //endregion Properties

    //region Public attributes
    public IBenchmarkSettingsModel Settings = new DefaultBenchmarkSettingsModel();
    //endregion Public attributes

    //region Run engines

    public void setStarted(boolean running) {

        if(running != mRunning) {
            mRunning = running;
            setChanged();
        }
        // Note: not thread safe
        if (running) {
            startEngines();
        } else {
            stopEngines();
        }
        notifyObservers();
    }

    /**
     * Getter for engines started
     * @return True if engines started, else false
     */
    public boolean getStarted() {
        return mRunning;
    }
    //endregion Run engines

    //region Private methods

    /**
     * Runs all selected benchmarking engines in new thread
     */
    private void startEngines() {
        mRunning = true;
        new Thread(new Runnable() {
            @Override
            public void run() {

                // benchmark all selected engines
                DatabaseHandler dbh = new DatabaseHandler(ObjectContainer.Default.getAppContext());
                for (IBenchmarkEngine engine : getEngines()) {
                    if(EngineSelection.get(engine)) {
                        try {
                            setStatus(String.format(new Date().toString() + ": Started %s", engine.getName()));
                            mRunningEngine = engine;
                            engine.startBenchmarking(Settings);
                            if(!getRunning()) {
                                setStatus(new Date().toString() + ": Benchmarking interrupted");
                                setStarted(false);
                                return;  // In case stopped, don't persist
                            }
                            engine.persist(dbh);

                        } catch (Exception e) {
                            MessengerService.Default.send(new ErrorMessage(e.getMessage()));
                        }
                    }
                }
                setStatus(new Date().toString() + ": Completed all benchmark engines");
                setStarted(false);
            }
        }).start();
    }

    /**
     * Stops the thread that was started in startEngines()
     */
    private void stopEngines() {
        mRunningEngine.stopBenchmarking();
    }

    private boolean getRunning() { return mRunning; }

    //endregion Private methods
}

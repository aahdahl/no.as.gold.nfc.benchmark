package no.as.gold.nfc.benchmark.controller;

import android.content.Context;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

import no.as.gold.nfc.benchmark.controller.messages.NewIntentMessage;
import no.as.gold.nfc.benchmark.view.R;
import no.as.gold.nfc.communication.NfcComHandler;
import no.as.gold.nfc.communication.Utils.Identifiers;
import no.as.gold.nfc.communication.messages.ComHandlerMessage;
import no.as.gold.simplemessenger.MessageHandler;
import no.as.gold.simplemessenger.MessengerService;
import no.as.gold.simplemessenger.messages.InformationMessage;

/**
 * This class performs binary read operations from tags
 * Created by Aage Dahl on 14.02.14.
 */
public class BReadTagFragment extends BaseTagFragment {
    private EditText mOffsetTextView;
    private EditText mReadLengthTextView;
    private TextView mReadTextView;
    private View mRootView;
    private Button mReadButton;
    private String mSelectedTech;

    private Collection<TextView> mTechs = new ArrayList<TextView>();
    private ListView mTechList;
    private Context mContext;
    private NfcAdapter mAdapter;
    private UUID mUUID = UUID.fromString(Identifiers.BReaderFragmentID);

    //region Fragment overrides
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Initiate view
        mRootView = inflater.inflate(R.layout.fragment_bread_tag,
                container, false);

        mReadTextView = (TextView)mRootView.findViewById(R.id.tag_read_content_text);
        mReadButton = (Button)mRootView.findViewById(R.id.read_tag_button);
        mOffsetTextView = (EditText)mRootView.findViewById(R.id.offset_text_view);
        mReadLengthTextView = (EditText)mRootView.findViewById(R.id.read_length_text_view);
        mTechList = (ListView)mRootView.findViewById(R.id.tech_list);
        mTechList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                 mSelectedTech = ((TextView)view).getText().toString();
            }
        });

        mContext = mRootView.getContext();

        // Get nfc adapter
        mAdapter = NfcAdapter.getDefaultAdapter(mContext);

        // Set button listener
        mReadButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Read tag content and output it to mReadTextView
                readTag();
            }
        });

        // Register listeners for incoming ComHandlerMessages
        MessengerService.Default.Register(this, ComHandlerMessage.class, new MessageHandler<ComHandlerMessage>() {
            @Override
            public void handle(final ComHandlerMessage message) {
            // Only listen to the messages sent by me!
            if(mUUID.equals(message.getSenderUUID())) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mReadTextView.setText(message.getMessage());
                    }
                });
            }
            }
        });

        // Listen for new tags
        MessengerService.Default.Register(this, NewIntentMessage.class, new MessageHandler<NewIntentMessage>() {
            @Override
            public void handle(final NewIntentMessage msg) {
                // Update tech list
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Tag tag = msg.Intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
                        if(tag == null) return;

                        // Set new adapter and add list of items
                        ArrayAdapter<String> adapter = new ArrayAdapter<>(mContext, android.R.layout.simple_list_item_1, tag.getTechList());
                        mTechList.setAdapter(new ArrayAdapter<String>(mContext, android.R.layout.simple_list_item_1, tag.getTechList()));
                    }
                });
            }
        });

        return mRootView;
    }

    private void readTag() {
        try {
            int length = Integer.valueOf(mReadLengthTextView.getText().toString());
            int offset = Integer.valueOf(mOffsetTextView.getText().toString());
            NfcComHandler.ByteComHandler.Read(GetTag(), mSelectedTech ,mUUID, offset, length);
        } catch (Exception e) {
            MessengerService.Default.send(new InformationMessage(e.toString()));
        }
    }

    //endregion

}

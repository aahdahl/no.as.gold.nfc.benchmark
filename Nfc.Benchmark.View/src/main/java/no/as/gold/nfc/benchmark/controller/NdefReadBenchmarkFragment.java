package no.as.gold.nfc.benchmark.controller;


import android.content.Context;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TableLayout;
import android.widget.ToggleButton;

import java.util.UUID;

import no.as.gold.nfc.benchmark.controller.engines.IComBenchmarkEngine;
import no.as.gold.nfc.benchmark.controller.engines.IBenchmarkResultHandler;
import no.as.gold.nfc.benchmark.controller.engines.NdefReadBenchmarkEngine;
import no.as.gold.nfc.benchmark.view.R;
import no.as.gold.nfc.communication.Utils.Identifiers;
import no.as.gold.simplemessenger.MessengerService;
import no.as.gold.simplemessenger.messages.InformationMessage;

/**
 * Created by aage on 16.10.13.
 * This fragment allows the user to administer benchmark testing
 * The extension of {@link AbstractBenchmarkResultsFragment} gives access and update capabilities of gui through the table
 */
public class NdefReadBenchmarkFragment extends AbstractBenchmarkResultsFragment {

        private static String NAME = "NdefReadBenchmarkFragment";

        //region Fields
        View mRootView;
        private ToggleButton mBenchmarkButton;
        private Context mContext;
        private NfcAdapter mAdapter;
        private UUID mReadUUID = UUID.fromString(Identifiers.NdefReadBenchmarkReaderID);
        private UUID mWriteUUID = UUID.fromString(Identifiers.NdefReadBenchmarkWriterID);
        private BenchmarkSettingsFragment mSettingsFragment = new BenchmarkSettingsFragment();
        private TableLayout mTable;
        private ToggleButton mSimulationButton;
        //endregion

        //region AbstractBenchmarkResultsFragment implementation
        @Override
        public TableLayout getTable() {
            return mTable;
        }

        @Override
        public Context getContext() {
            return mContext;
        }
        //endregion

        //region Properties
        private void setTable(TableLayout table) {
            mTable = table;
        }

        // Fragment containing the settings
        public BenchmarkSettingsFragment getSettingsFragment() {
            return mSettingsFragment;
        }
        private void setSettingsFragment(BenchmarkSettingsFragment mSettingsFragment) {
            this.mSettingsFragment = mSettingsFragment;
        }

        // Button activating the simulation
        public ToggleButton getSimulationButton() {
            return mSimulationButton;
        }
        private void setSimulationButton(ToggleButton simulationButton) {
            mSimulationButton = simulationButton;
        }

        public UUID getWriteUUID() {
            return mWriteUUID;
        }
        public UUID getReadUUID() {
            return mReadUUID;
        }


        //endregion

        //region Fragment overrides
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

            // Initiate view
            mRootView = inflater.inflate(R.layout.fragment_benchmark,
                    container, false);

            setTable((TableLayout) mRootView.findViewById(R.id.results_table_layout));
            mBenchmarkButton = (ToggleButton)mRootView.findViewById(R.id.benchmark_toggle_button);
            mSimulationButton = (ToggleButton)mRootView.findViewById(R.id.simulation_button);

            mContext = mRootView.getContext();

            // Get nfc adapter
            mAdapter = NfcAdapter.getDefaultAdapter(mContext);

            // Set button listener
            mBenchmarkButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                IComBenchmarkEngine engine;
                @Override
                public void onCheckedChanged(final CompoundButton compoundButton, boolean started) {
                    if(started) {
                        // Clear current view
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                getTable().removeAllViews();
                            }
                        });

                        engine = new NdefReadBenchmarkEngine(getReadUUID(), getWriteUUID());

                        // Update GUI when new sample arrives
                        engine.setSampleHandler(new IBenchmarkResultHandler() {
                            @Override
                            public void run() {
                                updateGui(engine.getSamples());
                            }
                        });

                        // Set button to off state when finished
                        engine.setBenchmarkCompletedHandler(new IBenchmarkResultHandler() {
                            @Override
                            public void run() {
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        compoundButton.setChecked(false);
                                    }
                                });
                            }
                        });

                        // start benchmarking
                        try {
                            engine.startBenchmarking(mSettingsFragment.GetSettings());
                        }catch (Exception e) {
                            // If error in startup -> report!
                            MessengerService.Default.send(new InformationMessage(e.getMessage()));
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    compoundButton.setChecked(false);
                                }
                            });
                        }
                    } else {
                        // Stop benchmarking
                        engine.stopBenchmarking();
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                compoundButton.setChecked(false);
                            }
                        });
                    }
                }
            });

            // Set simulation button
            mSimulationButton.setOnCheckedChangeListener(new SimulationButtonListener(this));

            // Register message listeners
            registerMessageListeners();

            return mRootView;
        }

        @Override
        public void onResume() {
            super.onResume();
            getFragmentManager().beginTransaction().add(R.id.fragment_container_layout, mSettingsFragment).commit();
        }

        @Override
        public void onPause() {
            super.onPause();
            getFragmentManager().beginTransaction().remove(mSettingsFragment).commit();
        }
        //endregion Fragment overrides

        //region Private methods
        private void registerMessageListeners() {
        }
        //endregion

        //region public functions

        //endregion public classes


}

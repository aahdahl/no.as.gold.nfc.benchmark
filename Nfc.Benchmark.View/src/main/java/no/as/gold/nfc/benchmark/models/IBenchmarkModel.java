package no.as.gold.nfc.benchmark.models;

import no.as.gold.nfc.benchmark.domain.IComBenchmarkSample;
import no.as.gold.nfc.benchmark.domain.IComBenchmarkSampleCollection;

/**
 * Contract for functionality of the class handling the information from a benchmark run.
 * Created by aage on 13.12.13.
 */
public interface IBenchmarkModel {
    enum TagType {
        TAG_TYPE1, TAG_TYPE2, TAG_TYPE3, TAG_TYPE4
    }

    /**
     * Sets the settings of this benchmark
     * @param settings Settings of this benchmark
     */
    public void setBenchmarkSettings(IBenchmarkSettingsModel settings);

    /**
     * Gets the settings of this benchmark
     * @return Settings of this benchmark
     */
    public IBenchmarkSettingsModel getBenchmarkSettings();

    /**
     * Adds a sample to this benchmark.
     * @param sample Sample to be added.
     */
    public void addSample(IComBenchmarkSample sample);

    /**
     * Removes a sample from this benchmark.
     * @param sample Sample to be removed.
     */
    public void removeSample(IComBenchmarkSample sample);

    /**
     * Returns all samples of the given size.
     * @param size Size of samples to return.
     * @return Collections of samples with the given size.
     */
    public IComBenchmarkSampleCollection getSamplesOfSize(int size);

    /**
     * Sets the tag type tested in this benchmarking
     * @param type Type of tag tested.
     */
    public void setTagType(TagType type);

    /**
     * Gets the type of tag used for this benchmark
     * @return
     */
    public TagType getTagType();
}

package no.as.gold.nfc.benchmark.controller.engines;

import android.nfc.Tag;
import android.nfc.tech.IsoDep;
import android.nfc.tech.MifareClassic;
import android.nfc.tech.MifareUltralight;
import android.nfc.tech.Ndef;
import android.nfc.tech.NfcA;
import android.nfc.tech.NfcB;
import android.nfc.tech.NfcF;
import android.nfc.tech.NfcV;
import android.nfc.tech.TagTechnology;

import org.apache.commons.lang3.RandomStringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import java.util.UUID;
import java.util.zip.CRC32;

import no.as.gold.nfc.benchmark.domain.DefaultComBenchmarkSample;
import no.as.gold.nfc.benchmark.domain.DefaultComBenchmarkSampleCollection;
import no.as.gold.nfc.benchmark.domain.IComBenchmarkSample;
import no.as.gold.nfc.benchmark.models.IBenchmarkSettingsModel;
import no.as.gold.nfc.benchmark.persistence.DatabaseHandler;
import no.as.gold.nfc.benchmark.utils.ObjectContainer;
import no.as.gold.nfc.communication.NfcComHandler;
import no.as.gold.nfc.communication.Utils.Identifiers;
import no.as.gold.nfc.communication.messages.ComHandlerMessage;
import no.as.gold.nfc.communication.technologies.ITagTechnologyWrapper;
import no.as.gold.nfc.communication.technologies.WrapperFactory;
import no.as.gold.simplemessenger.MessageHandler;
import no.as.gold.simplemessenger.MessengerService;
import no.as.gold.simplemessenger.messages.InformationMessage;

/**
 * Created by Aage Dahl on 13.02.14.
 */
public class ReadBenchmarkEngine extends AbstractComBenchmarkEngine {
    private Object mKey = new Object();
    private static final String mName = "Read benchmark";

    /**
     * Instantiates the runBenchmark with unique read and write {@link java.util.UUID}.
     *
     * @param readUUID  Used to distinguish read messages
     * @param writeUUID Used to distinguish write messages
     * @throws IllegalArgumentException if readUUID or writeUUID is null.
     */
    protected ReadBenchmarkEngine(UUID readUUID, UUID writeUUID) throws IllegalArgumentException {
        super(readUUID, writeUUID);
    }

    protected void runBenchmark(IBenchmarkSettingsModel settings, Tag tag, String technology) {
        // Assert parameters
        if(settings == null) throw new IllegalArgumentException("settings");
        if(tag == null) throw new IllegalArgumentException("tag");
        if(technology == null) throw new IllegalArgumentException("technology");

        //get technologies
        ArrayList<String> techs = new ArrayList<String>();
        String description = "";
        for(String tech : tag.getTechList()) {
            description += tech + "\n";
            techs.add(tech);
        }
        // Assert that tag has the given technology
        if(!techs.contains(technology)) throw new IllegalArgumentException("tag does not support this technology: " + technology);

        TagTechnology tagTechnology;
        switch (technology) {
            case Identifiers.TECH_NFC_A: tagTechnology = NfcA.get(tag); break;
            case Identifiers.TECH_NFC_B: tagTechnology = NfcB.get(tag); break;
            case Identifiers.TECH_NFC_F: tagTechnology = NfcF.get(tag); break;
            case Identifiers.TECH_NFC_V: tagTechnology = NfcV.get(tag); break;
            case Identifiers.TECH_ISO_DEP: tagTechnology = IsoDep.get(tag); break;
            case Identifiers.TECH_NDEF: tagTechnology = Ndef.get(tag); break;
            case Identifiers.TECH_MIFARE_CLASSIC: tagTechnology = MifareClassic.get(tag); break;
            case Identifiers.TECH_MIFARE_ULTRALIGHT: tagTechnology = MifareUltralight.get(tag); break;
            default: throw new IllegalArgumentException("technology: " + technology);
        }

        // Continuous read
        MessengerService.Default.Register(mKey, ComHandlerMessage.class, new MessageHandler<ComHandlerMessage>() {
            int mInterval;
            int mStartSize;
            int mSteps;
            int mReps;
            int mMessageCount = 0;
            Tag mTag;
            public ArrayList<CRC32> readCRCs = new ArrayList<CRC32>();
            DefaultComBenchmarkSample mSample;
            public TagTechnology mTech;

            /**
             * Initializing the anonymous class
             * @param settings settings to use for the test
             * @param tag mTag to communicate with
             * @return Instance of anonymous class
             */
            MessageHandler<ComHandlerMessage> initialize(IBenchmarkSettingsModel settings, Tag tag, TagTechnology tech) {
                mInterval = settings.getIntervalSize();
                mStartSize = settings.getStartSize();
                mSteps = (settings.getStopSize() - settings.getStartSize())/ mInterval;
                mReps = settings.getRepetitions();
                mTag = tag;
                mTech = tech;
                return this;
            }

            @Override
            public void handle(ComHandlerMessage msg) {
                if(msg.getSenderUUID().equals(mWriteUUID))
                    handleWrite(msg);
                else if(msg.getSenderUUID().equals(mReadUUID))
                    handleRead(msg);
            }

            /**
             * Handles reading, updates GUI and logs results
             * @param msg
             */
            private void handleRead(ComHandlerMessage msg) {
                if (msg.Status == ComHandlerMessage.StatusOptions.READ_SUCCESS) {
                    mSample.setSampleStatus(IComBenchmarkSample.Status.Succeed);
                    addSample();
                    writeNext();
                } else if (msg.Status == ComHandlerMessage.StatusOptions.CONNECTION_ERROR) {
                    NfcComHandler.ByteComHandler.Read(mTag, mTag.getTechList()[0], mReadUUID, 0,  mStartSize +  (mMessageCount*mInterval/ mReps));
                } else if (msg.Status == ComHandlerMessage.StatusOptions.READ_FAILED) {
                    // If CRC already exist -> highly likely that write failed
                    for(CRC32 crc : readCRCs) {
                        if(crc.getValue() == msg.getCRC().getValue()) {
                            mSample.setSampleStatus(IComBenchmarkSample.Status.Fail);
                            addSample();
                            writeNext();
                            return;
                        }
                    }
                    // if not -> read again
                    readCRCs.add(msg.getCRC());
                    NfcComHandler.ByteComHandler.Read(mTag, mTag.getTechList()[0], mReadUUID, 0, mStartSize + (mMessageCount*mInterval/ mReps));
                }
            }

            /**
             * Handles responce to writing to the mTag
             * @param msg
             */
            private void handleWrite(ComHandlerMessage msg) {
                Date timeStamp = new Date();
                double duration = msg.TransferTimeInSeconds;
                int size = msg.TransferSizeInBytes;
                mSample =  new DefaultComBenchmarkSample("ReadBenchmark", WrapperFactory.getTechnology(mTag).toString(), timeStamp, size, duration, null);

                if(msg.Status.equals(ComHandlerMessage.StatusOptions.WRITE_SUCCESS)) {
                    // Assert correct write by reading
                    readCRCs.clear();
                    NfcComHandler.ByteComHandler.Read(mTag, mTag.getTechList()[0], mReadUUID, 0, mStartSize + (mMessageCount*mInterval/ mReps));
                    return;
                }
                // Writing failed...
                if(msg.Status == ComHandlerMessage.StatusOptions.CONNECTION_ERROR) {
                    mSample.setSampleStatus(IComBenchmarkSample.Status.CommunicationError);
                } else {
                    mSample.setSampleStatus(IComBenchmarkSample.Status.Fail);
                }
                addSample();
                writeNext();
            }

            /**
             * Calculates the size of the next string and transfers it.
             * Stops when reaching the end of the benchmark
             */
            private void writeNext() {
                // If finished, stop listener and run completion handle
                if((mMessageCount >= mSteps * mReps)) {
                    MessengerService.Default.UnRegister(mKey, ComHandlerMessage.class);
                    mBenchmarkCompleteHandler.run();
                    return;
                }

                // new interval -> new sample collection
                if(mMessageCount % mReps == 0) {
                    mSamples.add(new DefaultComBenchmarkSampleCollection());
                }

                int stringLength = mStartSize +  (mMessageCount*mInterval/ mReps);
                NfcComHandler.ByteComHandler.Write(mTag, mWriteUUID, mTech, RandomStringUtils.randomAlphabetic(stringLength));
            }

            /**
             * Adds a sample to the collection based on the status of msg
             */
            private void addSample() {
                // add sample to current Benchmark collection
                mMessageCount++;
                MessengerService.Default.send(new InformationMessage(String.format("Progress: %d%%", 100 * mMessageCount/(mSteps * mReps))));
                mSamples.get(mSamples.size() - 1).add(mSample);

                // Run sample handle
                mSampleHandler.run();
            }
        }.initialize(settings, tag, tagTechnology));

        // Initial write!
        mSamples.add(new DefaultComBenchmarkSampleCollection());
        NfcComHandler.ByteComHandler.Write(tag, mWriteUUID, tagTechnology, RandomStringUtils.randomAlphabetic(settings.getStartSize()));

    }

    @Override
    protected void runBenchmark(IBenchmarkSettingsModel settings) {
        // Runs benchmarking on all tag technologies
        for(String tech : ObjectContainer.Default.getTag().getTechList()) runBenchmark(settings, ObjectContainer.Default.getTag(), tech);
    }

    protected void runBenchmark(ITagTechnologyWrapper wrapper, TagTechnology tech,  int intervals, int repetitions) {

        MessengerService.Default.Register(this, ComHandlerMessage.class, new MessageHandler<ComHandlerMessage>() {
            ITagTechnologyWrapper mWrapper;
            TagTechnology mTech;
            int mRepetitions;
            int mStartAddr;
            int mEndAddr;
            int mMemSize;
            int mInterval;
            int mReps = 0;
            int mIntervalCount = 0;
            @Override
            public void handle(ComHandlerMessage msg) {
                /*
                for(int i = 0; i < intervals; i++) {
                    // write byte[i*intervalSize]
                    for(int j = 0; j < repetitions; j++) {
                        // read byte[i*intervals]
                    }
                }
                */
                // Handle write responce
                if(mWriteUUID.equals(msg.getSenderUUID())) {
                    handleWrite(msg);
                } else if(mReadUUID.equals(msg.getSenderUUID())) {
                    handleRead(msg);
                } else return;

            }

            private void handleRead(ComHandlerMessage msg) {
                // Handle message responce
                switch (msg.Status) {
                    case READ_SUCCESS:

                        break;
                    case READ_FAILED:
                        break;
                    case CONNECTION_ERROR:
                        break;
                    default:

                }

                mIntervalCount++;
                // Handle new write operation
                if(mReps == mRepetitions) {
                    try {
                        mWrapper.write(mTech, makeRandomMessage(mInterval), mStartAddr);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            private void handleWrite(ComHandlerMessage msg) {
            }

            /**
             * Initialization of variables
             * @param wrapper
             * @param intervals
             * @param repetitions
             * @return this handler
             */
            public MessageHandler initialize(ITagTechnologyWrapper wrapper, TagTechnology tech, int intervals, int repetitions) {
                mWrapper = wrapper;
                mTech = tech;
                mStartAddr = wrapper.getMinContiguousRWAddress();
                mEndAddr = wrapper.getMaxContiguousRWAddress();
                mMemSize = mEndAddr - mStartAddr;
                mInterval = mMemSize / intervals;
                mRepetitions = repetitions;
                return this;
            }
        }.initialize(wrapper, tech, intervals, repetitions));

        // Initial message will write simple message to the framework
        try {
            int startSize = (wrapper.getMaxContiguousRWAddress() - wrapper.getMinContiguousRWAddress()) / intervals;
            wrapper.write(tech, makeRandomMessage(startSize), wrapper.getMinContiguousRWAddress());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void stopBenchmarking() {

    }

    @Override
    public void persist(DatabaseHandler dbh) {
        throw new UnsupportedOperationException("persist");
    }

    @Override
    public String getName() {
        return mName;
    }

    /**
     * Creates a random byte array
     * @param size Size of byte array
     * @return Randomized byte array
     */
    private byte[] makeRandomMessage(int size) {
        byte[] bytes = new byte[size];
        new Random(new Date().getTime()).nextBytes(bytes);
        return bytes;
    }
}

package no.as.gold.nfc.benchmark.controller.engines;

import java.util.ArrayList;

import no.as.gold.nfc.benchmark.domain.IComBenchmarkSampleCollection;

/**
 * This is an interface that describes the basic functionality of a benchmark engine. This engine performs benchmarking communication with a NFC tag.
 *
 * Created by Aage Dahl on 07.02.14.
 */
public interface IComBenchmarkEngine<T extends IBenchmarkResultHandler> extends IBenchmarkEngine {

    /**
     * Callback handle that is executed when each sample is collected.
     * Optional.
     * @param handler Callback function for handling each individual sample
     */
    void setSampleHandler(T handler);

    /**
     * Gets the collection of samples that have been collected up until now.
     * @return Collection of benchmark samples from this execution
     */
    ArrayList<IComBenchmarkSampleCollection> getSamples();

}

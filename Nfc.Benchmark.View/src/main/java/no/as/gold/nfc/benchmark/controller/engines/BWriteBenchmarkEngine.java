package no.as.gold.nfc.benchmark.controller.engines;

import android.nfc.Tag;
import android.nfc.TagLostException;
import android.nfc.tech.TagTechnology;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.UUID;

import no.as.gold.nfc.benchmark.domain.DefaultComBenchmarkSample;
import no.as.gold.nfc.benchmark.domain.DefaultComBenchmarkSampleCollection;
import no.as.gold.nfc.benchmark.domain.IComBenchmarkSample;
import no.as.gold.nfc.benchmark.domain.IComBenchmarkSampleCollection;
import no.as.gold.nfc.benchmark.models.IBenchmarkSettingsModel;
import no.as.gold.nfc.benchmark.persistence.DatabaseHandler;
import no.as.gold.nfc.benchmark.utils.ObjectContainer;
import no.as.gold.nfc.communication.Utils.Identifiers;
import no.as.gold.nfc.communication.Utils.Utils;
import no.as.gold.nfc.communication.technologies.ITagTechnologyWrapper;
import no.as.gold.nfc.communication.technologies.WrapperFactory;
import no.as.gold.simplemessenger.MessengerService;
import no.as.gold.simplemessenger.messages.ErrorMessage;
import no.as.gold.simplemessenger.messages.InformationMessage;

/**
 * Created by Aage Dahl on 09.03.14.
 */
public class BWriteBenchmarkEngine extends AbstractComBenchmarkEngine {
    private boolean mBenchmarkStopped;
    private static final String mName = "Binary write benchmark";

    /**
     * Instantiates the runBenchmark with unique read and write {@link java.util.UUID}.
     *
     * @param readUUID  Used to distinguish read messages
     * @param writeUUID Used to distinguish write messages
     * @throws IllegalArgumentException if readUUID or writeUUID is null.
     */
    public BWriteBenchmarkEngine(UUID readUUID, UUID writeUUID) throws IllegalArgumentException {
        super(readUUID, writeUUID);
    }

    /**
     * Instantiates binary write benchmark engine with default read and write UUID's
     */
    public BWriteBenchmarkEngine() {
        this(UUID.fromString(Identifiers.BWriteBenchmarkReaderID), UUID.fromString(Identifiers.BWriteBenchmarkWriterID));
    }

    @Override
    protected void runBenchmark(IBenchmarkSettingsModel settings) {
        mBenchmarkStopped = false;
        String techName;
        int intervals = 0;
        // Initialize - get the wrapper to extract information about tag size etc.
        ITagTechnologyWrapper wrapper;
        try {
            wrapper = initialize();
            techName = WrapperFactory.getTechnology(ObjectContainer.Default.getTag()).getClass().getName();
            intervals = (wrapper.getMaxContiguousRWAddress() - wrapper.getMinContiguousRWAddress()) / settings.getIntervalSize();
        } catch (Exception e) {
            // unable to initialize
            MessengerService.Default.send(new ErrorMessage("Unable to make contact with tag."));
            return;
        }

        int startAddr = wrapper.getMinContiguousRWAddress();
        int endAddr = wrapper.getMaxContiguousRWAddress();
        String timerId = UUID.randomUUID().toString();

        // For each interval
        int size = settings.getIntervalSize();
        while(size < endAddr - startAddr) {
            IComBenchmarkSampleCollection samples = new DefaultComBenchmarkSampleCollection();
            getSamples().add(samples);
            byte[] data = makeRandomMessage(size);
            // For each repetition
            for(int i = 0; i < settings.getRepetitions(); i++) {
                try {
                    wrapper.connect(WrapperFactory.getTechnology(ObjectContainer.Default.getTag()));
                    Utils.SimpleTimer.startTimer(timerId);
                    wrapper.write(data, wrapper.getMinContiguousRWAddress());
                    double duration = Utils.SimpleTimer.getElapsedSeconds(timerId);
                    // Assert that information was written
                    byte[] read_data = wrapper.read(data.length, wrapper.getMinContiguousRWAddress());
                    if(Arrays.equals(read_data, data)) {
                        // success
                        samples.add(new DefaultComBenchmarkSample(settings.getName(), techName, new Date(), size, duration, IComBenchmarkSample.Status.Succeed));
                    } else {
                        // failed
                        samples.add(new DefaultComBenchmarkSample(settings.getName(), techName, new Date(), size, duration, IComBenchmarkSample.Status.Fail));
                    }
                } catch (Exception e) {
                    double duration = Utils.SimpleTimer.getElapsedSeconds(timerId);
                    e.printStackTrace();
                    // Communication error
                    samples.add(new DefaultComBenchmarkSample(settings.getName(), techName, new Date(), size, duration, IComBenchmarkSample.Status.CommunicationError));
                } finally {
                    // cleanup
                    Utils.SimpleTimer.removeTimer(timerId);
                    try {
                        wrapper.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                // Check if process is stopped
                if(mBenchmarkStopped) return;

                // Progress report
                if(mSampleHandler != null)
                    mSampleHandler.run();

                MessengerService.Default.send(new InformationMessage(String.format("BWrite progress: size %d/%d, repetition %d/%d", size, intervals, i, settings.getRepetitions())));
            }
            // increment size
            size += settings.getIntervalSize();
        }

        //runBenchmark(settings.getName(), WrapperFactory.getWrapper(tag), WrapperFactory.getTechnology(tag), settings.getIntervalSize(), settings.getRepetitions());
        mBenchmarkStopped = true;
    }

    /**
     * Tries to locate the tag and return a wrapper.
     * @return Wrapper for tag technology
     * @throws Exception If unable to create wrapper after trying 100 times
     */
    private ITagTechnologyWrapper initialize() throws Exception {
        for (int i = 0; i < 100; i++) {
            try {
                return WrapperFactory.getWrapper(ObjectContainer.Default.getTag());
            } catch (Exception e) {
                try {
                    Thread.sleep(100, 0);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        }
        throw new Exception("Unable to create wrapper");
    }

    private void runBenchmark(String benchmarkName, ITagTechnologyWrapper wrapper, TagTechnology tech, int intervalSize, int repetitions) {
        IComBenchmarkSampleCollection samples;
        int startAddr = wrapper.getMinContiguousRWAddress();
        int endAddr = wrapper.getMaxContiguousRWAddress();
        int intervals = (endAddr - startAddr) / intervalSize;

        // Initialize by connecting to technology
        try {
            wrapper.connect(tech);
        } catch (IOException e) {
            MessengerService.Default.send(new ErrorMessage("BWriteBenchmarkEngine.runBenchmark(..) unable to connect"));
            e.printStackTrace();
            try {
                wrapper.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return;
        }

        // Perform interval sampling
        for(int i = 0; i < intervals; i++) {
            int size = intervalSize * (i+1);
            byte[] data = makeRandomMessage(size);

            // Add sample collection
            samples = new DefaultComBenchmarkSampleCollection();
            getSamples().add(samples);

            // Perform repetitive reads and log results
            for(int j = 0; j < repetitions; j++) {
                String timerId = UUID.randomUUID().toString();
                Utils.SimpleTimer.startTimer(timerId);
                double elapsedTime;
                try {
                    wrapper.write(data, startAddr);
                    elapsedTime = Utils.SimpleTimer.getElapsedSeconds(timerId);
                    samples.add(new DefaultComBenchmarkSample(benchmarkName, tech.getClass().getName(), new Date(), size, elapsedTime, IComBenchmarkSample.Status.Succeed));
                } catch (TagLostException e) {      // Tag out of reach -> com. error
                    elapsedTime = Utils.SimpleTimer.getElapsedSeconds(timerId);
                    samples.add(new DefaultComBenchmarkSample(benchmarkName, tech.getClass().getName(), new Date(), size, elapsedTime, IComBenchmarkSample.Status.CommunicationError));
                    e.printStackTrace();
                } catch (IOException e) {           // IO exception -> data transmission error.
                    elapsedTime = Utils.SimpleTimer.getElapsedSeconds(timerId);
                    samples.add(new DefaultComBenchmarkSample(benchmarkName, tech.getClass().getName(), new Date(), size, elapsedTime, IComBenchmarkSample.Status.Fail));
                } finally {
                    Utils.SimpleTimer.removeTimer(timerId);
                }

                // Run handler on new sample
                if(mSampleHandler != null) mSampleHandler.run();

                MessengerService.Default.send(new InformationMessage(String.format("BWrite progress: interval %d/%d, repetition %d/%d", i, intervals, j, repetitions)));

                // Handle if user pressed stop
                if(mBenchmarkStopped) {
                    MessengerService.Default.send(new InformationMessage("BWrite benchmark stopped."));
                    try {
                        wrapper.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return;
                }
            }
        }
        // Run completion handler
        if(mBenchmarkCompleteHandler != null) mBenchmarkCompleteHandler.run();
        try {
            wrapper.close();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    @Override
    public void stopBenchmarking() {
        mBenchmarkStopped = true;
    }

    @Override
    public void persist(DatabaseHandler dbh) throws Exception {
        if(getSamples() == null) return;
        for(IComBenchmarkSampleCollection collection : this.getSamples()) {
            for(IComBenchmarkSample sample : collection) {
                dbh.addWriteCommunicationSample(sample);
            }
        }
    }

    @Override
    public String getName() {
        return mName;
    }

    private byte[] makeRandomMessage(int size) {
        return new byte[size];
    }
}

package no.as.gold.nfc.benchmark.controller;

import android.nfc.Tag;
import android.support.v4.app.Fragment;

import no.as.gold.nfc.benchmark.utils.ObjectContainer;

/**
 * This is a base class for any fragment that works with tags. It extends the {@link android.support.v4.app.Fragment} with functionality to run tag intents.
 * Created by aage on 16.12.13.
 */
public abstract class BaseTagFragment extends Fragment {
    //region fields
    private Tag mTag;
    //endregion

    //region Constructors
    /**
     * Constructor that initiates the BaseTagFragment
     */
    public BaseTagFragment() {
        // Add message listeners
        registerMessageListeners();
    }
    //endregion

    //region Properties
    public Tag GetTag() {return ObjectContainer.Default.getTag();} //mTag;}
    //endregion

    //region Private methods
    private void registerMessageListeners() {
        /* Migrated to using ObjectContainer
        // Extract tag from intent
        MessengerService.Default.Register(this, NewIntentMessage.class, new MessageHandler<NewIntentMessage>() {

            @Override
            public void handle(NewIntentMessage msg) {
                Tag tag = msg.Intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
                if(tag != null) mTag = tag;
            }
        });
        */
    }
    //endregion
}

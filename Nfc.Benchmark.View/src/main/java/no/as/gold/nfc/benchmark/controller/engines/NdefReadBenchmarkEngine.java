package no.as.gold.nfc.benchmark.controller.engines;

import android.nfc.Tag;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.Date;
import java.util.UUID;

import no.as.gold.nfc.benchmark.domain.DefaultComBenchmarkSample;
import no.as.gold.nfc.benchmark.domain.DefaultComBenchmarkSampleCollection;
import no.as.gold.nfc.benchmark.domain.IComBenchmarkSample;
import no.as.gold.nfc.benchmark.models.IBenchmarkSettingsModel;
import no.as.gold.nfc.benchmark.persistence.DatabaseHandler;
import no.as.gold.nfc.benchmark.utils.ObjectContainer;
import no.as.gold.nfc.communication.NfcComHandler;
import no.as.gold.nfc.communication.messages.ComHandlerMessage;
import no.as.gold.nfc.communication.technologies.WrapperFactory;
import no.as.gold.simplemessenger.MessageHandler;
import no.as.gold.simplemessenger.MessengerService;
import no.as.gold.simplemessenger.messages.InformationMessage;

/**
 * Created by Aage Dahl on 07.02.14.
 */
public class NdefReadBenchmarkEngine extends AbstractComBenchmarkEngine {
    final Object mKey = new Object();
    private static final String mName = "NDEF read benchmark";


    /**
     * Instantiates the runBenchmark with unique read and write {@link java.util.UUID}.
     * @param readUUID Used to distinguish read messages
     * @param writeUUID Used to distinguish write messages
     * @throws java.lang.IllegalArgumentException if readUUID or writeUUID is null.
     */
    public NdefReadBenchmarkEngine(UUID readUUID, UUID writeUUID) {
        super(readUUID, writeUUID);
    }

    //region IComBenchmarkEngine implementation
    @Override
    public void stopBenchmarking() {
        MessengerService.Default.UnRegister(mKey, ComHandlerMessage.class);
        MessengerService.Default.send(new InformationMessage("Ndef read benchmark stopped"));
    }

    @Override
    public void persist(DatabaseHandler dbh) {
        throw new UnsupportedOperationException("No persistence implemented for NdefReadBenchmarkEngine");
    }

    @Override
    public String getName() {
        return mName;
    }

    //endregion IComBenchmarkEngine implementation

    //region AbstractComBenchmarkEngine implementation
    @Override
    protected void runBenchmark(final IBenchmarkSettingsModel settings) {
        Tag tag = ObjectContainer.Default.getTag();
        // Continuous read
        MessengerService.Default.Register(mKey, ComHandlerMessage.class, new MessageHandler<ComHandlerMessage>() {
            int mInterval;
            int mStartSize;
            int mSteps;
            int mReps;
            int mMessageCount = 0;
            Tag mTag;

            /**
             * Initializing the anonymous class
             * @param settings settings to use for the test
             * @param tag mTag to communicate with
             * @return Instance of anonymous class
             */
            MessageHandler<ComHandlerMessage> initialize(IBenchmarkSettingsModel settings, Tag tag) {
                mInterval = settings.getIntervalSize();
                mStartSize = settings.getStartSize();
                mSteps = (settings.getStopSize() - settings.getStartSize())/ mInterval;
                mReps = settings.getRepetitions();
                mTag = tag;
                return this;
            }

            @Override
            public void handle(ComHandlerMessage msg) {
                if(msg.getSenderUUID().equals(mWriteUUID))
                    handleWrite(msg);
                else if(msg.getSenderUUID().equals(mReadUUID))
                    handleRead(msg);
            }

            /**
             * Handles reading, updates GUI and logs results
             * @param msg
             */
            private void handleRead(ComHandlerMessage msg) {
                // Analyse result of read
                Date timeStamp = new Date();
                double duration = msg.TransferTimeInSeconds;
                int size = msg.TransferSizeInBytes;
                IComBenchmarkSample.Status status;

                if(msg.Status == ComHandlerMessage.StatusOptions.READ_SUCCESS)  // No CRC failure
                    status = IComBenchmarkSample.Status.Succeed;
                else if(msg.Status == ComHandlerMessage.StatusOptions.CONNECTION_ERROR)
                    status = IComBenchmarkSample.Status.CommunicationError;
                else
                    status = IComBenchmarkSample.Status.Fail;

                            // add sample to current Benchmark collection
                            mSamples.get(mSamples.size() - 1).add(new DefaultComBenchmarkSample(settings.getName(), WrapperFactory.getTechnology(mTag).toString(), timeStamp, size, duration, status));

                // Run sample handle
                mSampleHandler.run();
                // If finished, stop listener and run completion handle
                if((mMessageCount >= mSteps * mReps)) {
                    MessengerService.Default.UnRegister(mKey, ComHandlerMessage.class);
                    mBenchmarkCompleteHandler.run();
                } else if(mMessageCount % mReps == 0 && ! msg.Status.equals(ComHandlerMessage.StatusOptions.WRITE_SUCCESS)) {
                    int stringLength = mStartSize +  (mMessageCount * mInterval / mReps);
                    mSamples.add(new DefaultComBenchmarkSampleCollection());
                    NfcComHandler.NdefComHandler.Write(mTag, mWriteUUID, RandomStringUtils.randomAlphabetic(stringLength));
                } else {
                    // increment message count
                    mMessageCount++;
                    MessengerService.Default.send(new InformationMessage(String.format("Progress: %d%%", 100 * mMessageCount/(mSteps * mReps))));

                    // message received -> finished reading -> read again!
                    NfcComHandler.NdefComHandler.Read(mTag, mReadUUID);
                }
            }

            /**
             * Handles responce to writing to the mTag
             * @param msg
             */
            private void handleWrite(ComHandlerMessage msg) {
                // Initiate a read if successfully written to mTag, rewrite if not!
                if(msg.Status.equals(ComHandlerMessage.StatusOptions.WRITE_SUCCESS)) {
                    mMessageCount++;
                    MessengerService.Default.send(new InformationMessage(String.format("Progress: %d%%", 100 * mMessageCount/(mSteps * mReps))));
                    NfcComHandler.NdefComHandler.Read(mTag, mReadUUID);
                }
                else {
                    NfcComHandler.NdefComHandler.Write(mTag, mWriteUUID, RandomStringUtils.randomAlphabetic(mStartSize));
                }
            }
        }.initialize(settings, tag));

        // Initial write!
        mSamples.add(new DefaultComBenchmarkSampleCollection());
        NfcComHandler.NdefComHandler.Write(tag, mWriteUUID, RandomStringUtils.randomAlphabetic(settings.getStartSize()));

    }
    //endregion AbstractComBenchmarkEngine implementation
}

package no.as.gold.nfc.benchmark.controller.engines;

import android.nfc.Tag;
import android.nfc.tech.TagTechnology;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

import no.as.gold.nfc.benchmark.domain.DefaultComBenchmarkSample;
import no.as.gold.nfc.benchmark.domain.DefaultComBenchmarkSampleCollection;
import no.as.gold.nfc.benchmark.domain.IComBenchmarkSample;
import no.as.gold.nfc.benchmark.domain.IComBenchmarkSampleCollection;
import no.as.gold.nfc.benchmark.models.IBenchmarkSettingsModel;
import no.as.gold.nfc.benchmark.persistence.DatabaseHandler;
import no.as.gold.nfc.benchmark.utils.ObjectContainer;
import no.as.gold.nfc.communication.Utils.Identifiers;
import no.as.gold.nfc.communication.Utils.Utils;
import no.as.gold.nfc.communication.technologies.ITagTechnologyWrapper;
import no.as.gold.nfc.communication.technologies.WrapperFactory;
import no.as.gold.simplemessenger.MessengerService;
import no.as.gold.simplemessenger.messages.ErrorMessage;
import no.as.gold.simplemessenger.messages.InformationMessage;

/**
 * Created by aahdahl on 3/29/2014.
 */
public class BReadAllBenchmarkEngine extends AbstractComBenchmarkEngine {
    private static final String mName = "Binary readAll benchmark";
    private boolean mBenchmarkStopped;

    /**
     * Instantiates the runBenchmark with unique read and write {@link java.util.UUID}.
     *
     * @param readUUID  Used to distinguish read messages
     * @param writeUUID Used to distinguish write messages
     * @throws IllegalArgumentException if readUUID or writeUUID is null.
     */
    protected BReadAllBenchmarkEngine(UUID readUUID, UUID writeUUID) throws IllegalArgumentException {
        super(readUUID, writeUUID);
    }

    public BReadAllBenchmarkEngine() {
        super(UUID.fromString(Identifiers.BReadAllBenchmarkReaderID), UUID.fromString(Identifiers.BReadAllBenchmarkWriterID));
    }

    @Override
    protected void runBenchmark(IBenchmarkSettingsModel settings) {
        mBenchmarkStopped = false;
        // for each repetition: read
        int repetitions = settings.getRepetitions();
        int i = 0;
        byte[] bytesRead;
        String techName = "";
        String uuid = UUID.randomUUID().toString();
        IComBenchmarkSampleCollection samples = new DefaultComBenchmarkSampleCollection();
        getSamples().add(samples);

        ITagTechnologyWrapper wrapper = null;

        // Fill tag with random data - try 10 times before giving up
        byte[] data = new byte[0];
        for(int j = 0; j <= 100; j++) {
            try {
                wrapper = WrapperFactory.getWrapper(ObjectContainer.Default.getTag());
                TagTechnology tech = WrapperFactory.getTechnology(ObjectContainer.Default.getTag());
                techName = tech.getClass().getName();
                // Extract tag information
                data = makeRandomMessage(wrapper.getMaxContiguousRWAddress() - wrapper.getMinContiguousRWAddress());
                wrapper.connect(tech);
                wrapper.write(data, wrapper.getMinContiguousRWAddress());
                break;
            } catch (Exception e) {
                MessengerService.Default.send(new ErrorMessage(e.getMessage()));
                // Wait 0.5 seconds before retrying
                try {
                    Thread.sleep(100,0);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            } finally {
                try {
                    if(wrapper != null)
                        wrapper.close();
                } catch (IOException e) {
                    MessengerService.Default.send(new ErrorMessage(e.getMessage()));
                }
            }
            if(j == 100) {
                MessengerService.Default.send(new ErrorMessage("BReadAllBenchmarkEngine.runBenchmark(..) unable to write"));
                mBenchmarkCompleteHandler.run();
                return;
            }
        }

        // Read all data available on the tag
        for(;i < repetitions; i++) {
            try {
                Tag tag = ObjectContainer.Default.getTag();                 // Repeat each time in case tag is lost and reconnected
                TagTechnology tech = WrapperFactory.getTechnology(tag);
                wrapper.connect(tech);
                Utils.SimpleTimer.startTimer(uuid);
                bytesRead = wrapper.readAll();
                double elapsedTime = Utils.SimpleTimer.getElapsedSeconds(uuid);
                if(Arrays.equals(bytesRead, data)) {
                    samples.add(new DefaultComBenchmarkSample(settings.getName(), techName, new Date(), bytesRead.length, elapsedTime, IComBenchmarkSample.Status.Succeed));
                } else {
                    samples.add(new DefaultComBenchmarkSample(settings.getName(), techName, new Date(), bytesRead.length, elapsedTime, IComBenchmarkSample.Status.Fail));
                }
            } catch (Exception e) {
                double elapsedTime = Utils.SimpleTimer.getElapsedSeconds(uuid);
                samples.add(new DefaultComBenchmarkSample(settings.getName(), techName, new Date(), data.length, elapsedTime, IComBenchmarkSample.Status.CommunicationError));
            } finally {
                // unregister timer
                Utils.SimpleTimer.removeTimer(uuid);
                //disconnect
                try {
                    wrapper.close();
                } catch (IOException e) {
                    MessengerService.Default.send(new ErrorMessage(e.getMessage()));
                }
            }
            if(mBenchmarkStopped) {
                return;
            }

            // Update user
            MessengerService.Default.send(new InformationMessage(String.format("BReadAll progress: repetition %d/%d", i, settings.getRepetitions())));
        }

    }

    @Override
    public String getName() {
        return mName;
    }

    @Override
    public void stopBenchmarking() {
        mBenchmarkStopped = true;
    }

    @Override
    public void persist(DatabaseHandler dbh) throws Exception {
        for(IComBenchmarkSampleCollection samples : getSamples())
            for(IComBenchmarkSample sample : samples)
                dbh.addReadAllCommunicationSample(sample);
    }

    //region private functions
    /**
     * Creates a random byte array
     * @param size Size of byte array
     * @return Randomized byte array
     */
    private byte[] makeRandomMessage(int size) {
        byte[] bytes = new byte[size];
        new Random(new Date().getTime()).nextBytes(bytes);
        return bytes;
    }

}

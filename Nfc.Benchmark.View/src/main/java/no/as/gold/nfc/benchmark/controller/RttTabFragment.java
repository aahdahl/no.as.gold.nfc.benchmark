package no.as.gold.nfc.benchmark.controller;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.UUID;

import no.as.gold.nfc.benchmark.view.R;
import no.as.gold.nfc.communication.NfcComHandler;
import no.as.gold.nfc.communication.messages.ComHandlerMessage;
import no.as.gold.simplemessenger.MessageHandler;
import no.as.gold.simplemessenger.MessengerService;

/**
 * Created by Aage Dahl on 02.03.14.
 */
public class RttTabFragment extends BaseTagFragment {
    private View mRootView;
    private Button mCalcRttButton;
    private TextView mRttTextView;
    private Context mContext;
    private UUID mUUID = UUID.randomUUID();

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Initiate view
        mRootView = inflater.inflate(R.layout.fragment_rtt,
                container, false);
        mContext = inflater.getContext();

        mCalcRttButton = (Button)mRootView.findViewById(R.id.calc_rtt_button);
        mRttTextView = (TextView)mRootView.findViewById(R.id.tag_rtt_text);
        mCalcRttButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Execute run
                NfcComHandler.ByteComHandler.timeRTT(GetTag(), mUUID);

            }
        });

        MessengerService.Default.Register(this, ComHandlerMessage.class, new MessageHandler<ComHandlerMessage>() {
            @Override
            public void handle(final ComHandlerMessage msg) {
                if(mUUID.equals(msg.getSenderUUID())) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mRttTextView.setText(msg.getMessage());
                        }
                    });
                }
            }
        });

        return mRootView;
    }
}

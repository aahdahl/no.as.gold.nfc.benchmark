package no.as.gold.nfc.benchmark.view.gui.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentFilter.MalformedMimeTypeException;
import android.nfc.NfcAdapter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

import no.as.gold.nfc.benchmark.controller.BReadBenchmarkFragment;
import no.as.gold.nfc.benchmark.controller.BReadTagFragment;
import no.as.gold.nfc.benchmark.controller.BWriteBenchmarkFragment;
import no.as.gold.nfc.benchmark.controller.BWriteTagFragment;
import no.as.gold.nfc.benchmark.controller.BenchmarkSchedulerFragment;
import no.as.gold.nfc.benchmark.controller.NdefReadBenchmarkFragment;
import no.as.gold.nfc.benchmark.controller.NdefWriteBenchmarkFragment;
import no.as.gold.nfc.benchmark.controller.ReadTagFragment;
import no.as.gold.nfc.benchmark.controller.RttTabFragment;
import no.as.gold.nfc.benchmark.controller.ToolsTabFragment;
import no.as.gold.nfc.benchmark.controller.WriteTagFragment;
import no.as.gold.nfc.benchmark.controller.messages.NewIntentMessage;
import no.as.gold.nfc.benchmark.utils.ObjectContainer;
import no.as.gold.nfc.benchmark.view.gui.R;
import no.as.gold.nfc.communication.Utils.Identifiers;
import no.as.gold.nfc.communication.messages.ComHandlerMessage;
import no.as.gold.simplemessenger.MessageHandler;
import no.as.gold.simplemessenger.MessengerService;
import no.as.gold.simplemessenger.messages.ErrorMessage;
import no.as.gold.simplemessenger.messages.IMessage;
import no.as.gold.simplemessenger.messages.InformationMessage;

import static android.widget.Toast.makeText;

public class MainActivity extends FragmentActivity implements ActionBar.TabListener {

    //region Static fields
    public static final String MIME_TEXT_PLAIN = "text/plain";

    // GUI commands
    public static final String TOAST_MESSAGE = "toast_message";
    //endregion

    //region Private fields
    private NfcAdapter mAdapter;
    private Context mContext;
    private Handler mGuiHandler;
    private ComHandlerMessage mComHandlerMessage;
    private Toast mToaster;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private AsyncTask<String, String, String> mRefreshTabs;
    private TextView mLogTv;
    private String mLogText = "";
    //endregion

    //region Overrides: Activity class
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        mLogTv = (TextView)findViewById(R.id.log_text_view);

        // Get context and extract toaster
        mContext = getApplicationContext();
        // Make context available for the rest of the app.
        ObjectContainer.Default.setAppContext(mContext);
        mToaster = makeText(mContext, "", Toast.LENGTH_SHORT);

        // Prepare tabs
        refreshActionBar();

        // Initiate last intent
        onNewIntent(getIntent());

        // Check NFC compatibility
        checkAndAssignNfcAdapter();

        // Delegate GUI changes to GUI thread
        mGuiHandler = new Handler(new MainActivityGuiHandler());

        // Register listeners for messages
        registerMessageListeners();
    }

    @Override
    protected void onStop() {
        super.onStop();
        MessengerService.Default.UnRegisterAll();
        if(mToaster != null)
            mToaster.cancel();
    }

    @Override
    protected void onNewIntent(Intent intent) {

        // In case NFC adapter is not assigned (if Nfc adapter was turned off)
        if(mAdapter != null) checkAndAssignNfcAdapter();

        MessengerService.Default.send(new NewIntentMessage(intent));
    }

    @Override
    protected void onResume() {
        super.onResume();
        setupForegroundDispatch(this, mAdapter);
    }


    @Override
    protected void onPause() {
        super.onPause();
        stopForegroundDispatch(this, mAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    //endregion

    //region Implementation: ActionBar.TabListener
    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }
    //endregion

    //region Private methods
    /**
     *
     * @param activity
     * @param adapter
     */
    private void stopForegroundDispatch(Activity activity,
                                        NfcAdapter adapter) {
        adapter.disableForegroundDispatch(activity);

    }

    /**
     * Prevents a new instance of this activity to start for every intent received
     * @param activity
     * @param adapter
     */
    private void setupForegroundDispatch(final Activity activity,
                                         NfcAdapter adapter) {
        final Intent intent = new Intent(activity.getApplicationContext(), activity.getClass());
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        final PendingIntent pendingIntent = PendingIntent.getActivity(activity.getApplicationContext(), 0, intent, 0);
        IntentFilter[] filters = new IntentFilter[1];
        String[][] techList = new String[][]{};
        // Notice that this is the same filter as in our manifest.
        filters[0] = new IntentFilter();
        filters[0].addAction(NfcAdapter.ACTION_TECH_DISCOVERED);
        filters[0].addCategory(Intent.CATEGORY_DEFAULT);
        try {
            filters[0].addDataType(MIME_TEXT_PLAIN);
        } catch (MalformedMimeTypeException e) {
            throw new RuntimeException("Check your mime type.");
        }
//        adapter.enableForegroundDispatch(activity, pendingIntent, filters, techList);
        adapter.enableForegroundDispatch(activity, pendingIntent, null, null);
    }

    // Updates tabs
    private void refreshActionBar() {
        // Set up the action bar.
        final ActionBar actionBar = getActionBar();

        // Cleanup
        actionBar.removeAllTabs();

        // Add new tabs
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the app.
        // Need to clear all existing fragments from fragmentManager before populating new ones ( or else it remembers old fragments)
        //if (getSupportFragmentManager().getFragments() != null) getSupportFragmentManager().getFragments().clear();
        mSectionsPagerAdapter = new SectionsPagerAdapter(
                getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        // When swiping between different sections, select the corresponding
        // tab. We can also use ActionBar.Tab#select() to do this if we have
        // a reference to the Tab.
        mViewPager
                .setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
                    @Override
                    public void onPageSelected(int position) {
                        actionBar.setSelectedNavigationItem(position);
                    }
                });

        // For each of the sections in the app, add a tab to the action bar.
        for (int i = 0; i < mSectionsPagerAdapter.getCount()-1; i++) {
            // Create a tab with text corresponding to the page title defined by
            // the adapter. Also specify this Activity object, which implements
            // the TabListener interface, as the callback (listener) for when
            // this tab is selected.
            actionBar.addTab(actionBar.newTab()
                    .setText(mSectionsPagerAdapter.getPageTitle(i))
                    .setTabListener(this));
        }

        // Add tab for settings
        actionBar.addTab(actionBar.newTab()
                .setIcon(android.R.drawable.ic_menu_preferences)
                .setTabListener(this));

        mSectionsPagerAdapter.notifyDataSetChanged();

    }

    /**
     * Private function that checks and if possible, assigns NfcAdapter
     */
    private void checkAndAssignNfcAdapter() {
        mAdapter = NfcAdapter.getDefaultAdapter(getApplicationContext());
        if(mAdapter == null) {
            mToaster.setText("This device does not support NFC");
            mToaster.show();
            finish();
            return;
        } else if(!mAdapter.isEnabled()) {
            mToaster.setText("NFC is disabled!");
            mToaster.show();
        }
    }

    /**
     * This function registers interest for messages sent from other activities or fragments
     */
    private void registerMessageListeners() {
        // Handle read/write messages
        MessengerService.Default.Register(this, ComHandlerMessage.class, new MessageHandler<ComHandlerMessage>() {
            @Override
            public void handle(ComHandlerMessage msg) {
                String toastMessage;
                switch(msg.Status) {
                    case READ_SUCCESS:
                        toastMessage = "Read: " + msg.getReadMessage();
                        break;
                    case READ_FAILED:
                        toastMessage = "Error reading from tag.\n" + msg.getMessage();
                        break;
                    case WRITE_SUCCESS:
                        toastMessage = "Successfully wrote to tag.\n" + msg.getMessage();
                        break;
                    case WRITE_FAILED:
                        toastMessage = "Error writing to tag.\n" + msg.getMessage();
                        break;
                    case CONNECTION_ERROR:
                        toastMessage = "Unable to connect to tag.\n" + msg.getMessage();
                        break;
                    default:
                        toastMessage = "Unknown communication message received.\n" + msg.getMessage();
                }

                // Log results from read
                if(mLogText.length() > 100)
                    mLogText = mLogText.substring(0,50);
                mLogText = toastMessage + "\n" + mLogText;
                mLogTv.setText(mLogText);

                // If message is from a benchmark message.. do not display it!
                if ( UUID.fromString(Identifiers.NdefReadBenchmarkReaderID).equals(msg.getSenderUUID())
                        || UUID.fromString(Identifiers.NdefReadBenchmarkWriterID).equals(msg.getSenderUUID())
                        || UUID.fromString(Identifiers.NdefWriteBenchmarkReaderID).equals(msg.getSenderUUID())
                        || UUID.fromString(Identifiers.NdefWriteBenchmarkWriterID).equals(msg.getSenderUUID())) {
                    return;
                }

                Message message = new Message();
                Bundle bundle = new Bundle();
                bundle.putString(TOAST_MESSAGE, toastMessage);
                message.setData(bundle);
                mGuiHandler.sendMessage(message);
            }
        });

        // Publish MessengerService.InformationMessage to toast
        MessengerService.Default.Register(this, InformationMessage.class, new MessageHandler<InformationMessage>() {
            @Override
            public void handle(final InformationMessage msg) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mToaster.setText(msg.getMessage());
                        mToaster.show();
                    }
                });
            }
        });

        // Log errors
        MessengerService.Default.Register(this, ErrorMessage.class, new MessageHandler<ErrorMessage>() {
            @Override
            public void handle(ErrorMessage msg) {
                SimpleDateFormat sdfDate = new SimpleDateFormat("HH:mm:ss");
                Date now = new Date();
                String strDate = sdfDate.format(now);

                mLogText = strDate + ": " + msg.getMessage() + "\n" + mLogText;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mLogTv.setText(mLogText);
                    }
                });
            }
        });
    }

    //endregion

    // region SectionPagerAdapter class and MainActivityGuiHandler class
    /**
     * This class extends {@link android.support.v4.app.FragmentPagerAdapter} and enables changing between a collection of fragments
     */
    private class SectionsPagerAdapter extends FragmentPagerAdapter {

        private final CharSequence SCHEDULER_TAB = "SCHEDULER";
        private final CharSequence BINARY_READ_BENCHMARK_TAB = "BRBENCH";
        private final CharSequence BINARY_WRITE_BENCHMARK_TAB = "BWBENCH";
        private final CharSequence READ_TAB = "READ";
        private final CharSequence WRITE_TAB = "WRITE";
        private final CharSequence NDEF_READ_TAB = "RNDEF";
        private final CharSequence NDEF_WRITE_TAB = "WNDEF";
        private final CharSequence BINARY_READ_TAB = "BREAD";
        private final CharSequence BINARY_WRITE_TAB = "BWRITE";
        private final CharSequence RTT_TAB = "RTT";

        // Collection of fragments
        ArrayList<Fragment> mFragments = new ArrayList<Fragment>();

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
            mFragments.add(new BenchmarkSchedulerFragment());
            mFragments.add(new ReadTagFragment());
            mFragments.add(new WriteTagFragment());
            mFragments.add(new NdefReadBenchmarkFragment());
            mFragments.add(new NdefWriteBenchmarkFragment());
            mFragments.add(new BReadTagFragment());
            mFragments.add(new BWriteTagFragment());
            mFragments.add(new RttTabFragment());
            mFragments.add(new BReadBenchmarkFragment());
            mFragments.add(new BWriteBenchmarkFragment());
            mFragments.add(new ToolsTabFragment());
        }

        @Override
        public CharSequence getPageTitle(int i) {
            switch(i)
            {
                case 0: return SCHEDULER_TAB;
                case 1: return READ_TAB;
                case 2: return WRITE_TAB;
                case 3: return NDEF_READ_TAB;
                case 4: return NDEF_WRITE_TAB;
                case 5: return BINARY_READ_TAB;
                case 6: return BINARY_WRITE_TAB;
                case 7: return RTT_TAB;
                case 8: return BINARY_READ_BENCHMARK_TAB;
                case 9: return BINARY_WRITE_BENCHMARK_TAB;
                default: return "";
            }
        }

        @Override
        public Fragment getItem(int i) {
            if(i > getCount() - 1)
                return null;
            return mFragments.get(i);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }
    }

    /**
     * Class that handles messages in GUI thread
     */
    private class MainActivityGuiHandler implements Handler.Callback {
        @Override
        public boolean handleMessage(Message msg) {
            // Lazy method of updating GUI
            for(String key:msg.getData().keySet()) {
                // Display toast message
                if(key.equals(TOAST_MESSAGE)) {
                    mToaster.setText(msg.getData().getString(key));
                    mToaster.show();
                }

                // Handle other message types
            }


            return true;
        }
    }
    // endregion
}

